import React from "react";
import { Route, Redirect, Switch, HashRouter } from "react-router-dom";
import { Provider } from "react-redux";
import REDUX_STORE from "./redux/store";

// Not logged in part
import Register from "./initial/register/Register";
import Login from "./initial/login/Login";
import HomeUnlogged from "./initial/home/Home";

// Logged in part
import SinglePost from "./logged/singlepost/SinglePost";
import UserInfo from "./logged/user/info/Info";
import Person from "./logged/person/Person";
import HomeLogged from "./logged/home/Home";
import Chats from "./logged/chats/Chats";
import Specific from "./logged/chats/specific/Specific";

const RedirectToUserName = function (props) {
    return <Redirect to={`/${props.match.params.nickname}`} />
}

class App extends React.Component {
    render() {
        // The Home component is displayes whenever the path is undefined
        return <Provider store={REDUX_STORE}>
            <HashRouter>
                <Switch>
                    <Route exact path="/home" component={HomeUnlogged} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/chats" component={Chats} />
                    <Route exact path="/chats/:with" component={Specific} />
                    <Route exact path="/post/:id" component={SinglePost} />
                    <Route exact path="/:nickname" component={Person} />
                    <Route exact path="/:user/getinfo/:type" component={UserInfo} />
                    <Route exact path="/" component={HomeLogged} />
                    <Route exact path="/redirectusername/:nickname" component={RedirectToUserName} />
                    <Route path="*" component={() => (<Redirect to="/" />)} />
                </Switch>
            </HashRouter>
        </Provider>
    }
}

export default App;