import { CHANGE_NICKNAME, CHANGE_NAVBAR_VISIBILITY, CHANGE_LOGGED, CHANGE_OWN, HIDE_INFO, SHOW_INFO, CHANGE_NAVBAR_HEIGHT } from "./types";

const initialState = {
    nickname: "",
    logged: false,
    own: false,
    info: {
        showInfo: false,
        type: ""
    },
    navVisible: true,
    navbarHeight: 0
}


function mainReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGE_NICKNAME:
            return Object.assign({}, state, { nickname: action.value });

        case CHANGE_LOGGED:
            return Object.assign({}, state, { logged: action.value });

        case CHANGE_OWN:
            return Object.assign({}, state, { own: action.value });

        case HIDE_INFO:
            return Object.assign({}, state, { info: { showInfo: false, type: "" } });

        case SHOW_INFO:
            return Object.assign({}, state, { info: { showInfo: true, type: action.value } });

        case CHANGE_NAVBAR_VISIBILITY:
            return Object.assign({}, state, { navVisible: action.value });

        case CHANGE_NAVBAR_HEIGHT:
            return Object.assign({}, state, { navbarHeight: action.value });

        default:
            return state;
    }
}

export default mainReducer;