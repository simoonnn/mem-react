import { CHANGE_NICKNAME, CHANGE_LOGGED, CHANGE_OWN,SHOW_INFO, HIDE_INFO, CHANGE_NAVBAR_VISIBILITY, CHANGE_NAVBAR_HEIGHT } from "./types";

export function changeNickname(nickname) {
    return {
        type: CHANGE_NICKNAME,
        value: nickname
    }
}

export function changeLoggedIn(isLoggedIn) {
    return {
        type: CHANGE_LOGGED,
        value: isLoggedIn
    }
}

export function changeOwn(isOwn) {
    return {
        type: CHANGE_OWN,
        value: isOwn
    }
}

export function hideInfo() {
    return {
        type: HIDE_INFO
    }
} 

export function showInfo(kind) {
    return {
        type: SHOW_INFO,
        value: kind
    }
}

export function changeNavVis(show) {
    return {
        type: CHANGE_NAVBAR_VISIBILITY, 
        value: show
    }
}

export function changeNavHeight(value) {
    return {
        type: CHANGE_NAVBAR_HEIGHT,
        value: value
    }
}