import ReactDOM from "react-dom";
import React from "react";

import "./fonts";
import App from "./App";
import "./main-styles.scss";


const el = document.getElementById("container");

ReactDOM.render(<App/>, el);