// export const BACK_END = "http://192.168.1.119:9001";
export const BACK_END = "http://localhost:9001";

// Dates
export const SECOND = 1000;
export const MINUTE = SECOND * 60;
export const HOUR = MINUTE * 60;
export const DAY = HOUR * 24;  
export const LOGIN_COOKIE_EXPIRATION = DAY * 10000;

// Default User Avatar
export const DUA = "/assets/mock-avatar.jpeg";

// Posts
export const PPR = 7;
export const MPR = 30 ;

// textarea lines propertiese
export const maxLines = 6;

// max length of the message to display when replying
export const replyLength = 100;

// subscribers and subscriptions
export const SUBSCRIBERS = "SUBSCRIBERS";
export const SUBSCRIPTIONS = "SUBSCRIPTIONS";

// Defined scroll difference 
export const DSD = 200;

// Calendar 
export const CALENDAR = {
    0: "January",
    1: "February", 
    2: "March",
    3: "April",
    4: "May", 
    5: "June",
    6: "July", 
    7: "August",
    8: "September",
    9: "October",
    10: "November",
    11: "December"
}