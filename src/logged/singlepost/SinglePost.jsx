import React from "react";
import { connect } from "react-redux";

import Spinner from "initial/utils/spinner/Spinner";
import { BACK_END } from "presets";
import styles from "./styles.scss";
import Navbar from "utils/navbar/Navbar";
import Post from "../display/posts/post/Post";
import isLogged from "utils/logged/isLogged";

class SinglePost extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            post: {},
            norows: false,
            loading: true
        }
    }
    componentDidMount() {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                const response = JSON.parse(xhr.response);
                if (response.status == "success") {
                    setTimeout(() => {
                        this.setState({ post: response.post, loading: false });
                    }, 4000);
                } else {
                    this.setState({ norows: true, loading: false });
                }
            }
        }

        // extract the requested post id from url
        const id = this.props.match.params.id.toString();

        const body = JSON.stringify({
            id
        });

        xhr.open("POST", BACK_END + "/singlepost");
        xhr.send(body);
    }

    render() {
        const logged = isLogged();
        const navbarHeight = this.props.navbarHeight;

        const state = this.state;
        const post = state.post;
        const loaded = !state.loading && !state.norows;
        // decide what the padding should be
        const style = { paddingBottom: `${navbarHeight}px` };

        return <div style={style} className={styles.Container}>
            <h1>Single post</h1>
            <div className={styles.Post}>
                {state.loading ? <div className={styles.SpinnerContainer}>
                    <Spinner cls={styles.Spinner} />
                </div> : ""}
                {state.norows ?
                    <h1 className={styles.Header}>The post is not found</h1>
                    : ""}
                {loaded ? <Post key={post.id} logged={logged} liked={logged ? post.liked : false} shared={logged ? post.shared : false} object={post} /> : ""}
            </div>
            <Navbar />
        </div>
    }
}

function mapStateToProps(state) {
    return {
        navbarHeight: state.navbarHeight
    }
}

SinglePost = connect(mapStateToProps)(SinglePost);

export default SinglePost;