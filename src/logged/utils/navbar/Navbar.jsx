import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import styles from "./styles.scss";
import Control from "./control/Control";
import { changeNavHeight } from "src/redux/actions/";

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.prHeight = 0;

        // create ref
        this.container = React.createRef();
        this.handler = this.handler.bind(this);
    }

    handler() {
        const container = this.container.current;
        if (container) {
            const height = container.scrollHeight;
            if (height != this.prHeight) {
                this.props.setHeight(height);
            }
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.handler();
        }, 300);
        window.addEventListener("resize", this.handler);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handler);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.navVisible != this.props.navVisible || true) {
            const container = this.container.current;
            const height = container.scrollHeight;
            this.props.setHeight(height);
        }
    }

    render() {
        const redirect = this.props.redirect;
        const homelink = redirect ? "/" : "";
        const navbarVisible = this.props.navbarVisible;

        return <div ref={this.container} className={navbarVisible ? styles.Container : styles.Hidden}>
            <div className={styles.Flex}>
                <Control source="/assets/icons/navbar/news.svg" />
                <Control link={homelink} redirect={redirect} source="/assets/icons/navbar/home.svg" />
                <Control link="/chats" redirect={redirect} source="/assets/icons/navbar/chats.svg" />
            </div>
        </div>
    }
}

Navbar.propTypes = {
    redirect: PropTypes.bool
}

Navbar.defaultProps = {
    redirect: true
}

// Redux stuff
function mapStateToProps(state) {
    return {
        navbarVisible: state.navVisible
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setHeight: (value) => dispatch(changeNavHeight(value))
    }
}

Navbar = connect(mapStateToProps, mapDispatchToProps)(Navbar);

export default Navbar;
