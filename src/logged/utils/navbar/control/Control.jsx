import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import styles from "./styles.scss";


class Control extends React.Component {
    render() {
        const link = this.props.link;

        if (link) {
            return <div className={styles.Container}>
                <Link to={link}>
                    <div className={styles.stretch}>
                        <img src={this.props.source} />
                    </div>
                </Link>
            </div>
        }
        return <div className={styles.Disabled}>
            <img className={styles.Disabled} src={this.props.source} />
        </div>
    }
}

Control.propTypes = {
    source: PropTypes.string,
    link: PropTypes.string,
    redirect: PropTypes.bool
}


export default Control;