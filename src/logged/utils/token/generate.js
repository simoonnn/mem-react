function generateToken(length) {
    const chars = "qwertyuipasdfghjkzxcvbnm,/|123456789!@#$%^&*()ASDFGHJKL:ZXCVBNM<>";
    let seq = ""
    const cLength = chars.length;
    for (var i = 0; i < length; i++) {
        seq += chars[Math.floor(Math.random() * cLength)];
    }

    return seq;
}

export default generateToken;