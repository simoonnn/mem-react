export function getCookie(key) {
    const cookie = document.cookie + ";";
    //Index of the key
    let keyIndex = cookie.indexOf(key);
    if (keyIndex == -1) {
        return;
    }

    // Index of equal sign after key
    let esi = cookie.indexOf("=", keyIndex);

    // Index of semicolon after key
    let smi = cookie.indexOf(";", esi);
    if (smi == -1) {
        smi = cookie.indexOf("\n", esi)
    }

    return cookie.substring(esi + 1, smi).trim();
}