import {getCookie} from "../cookies/getCookie";


export default function isLogged() {
    const nickname = getCookie("nickname");
    const token = getCookie("token");

    return (!!nickname && !!token);
}