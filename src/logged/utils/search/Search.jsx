import React from "react";
import { connect } from "react-redux";

import styles from "./styles.scss";
import Results from "./results/Results";
import { BACK_END } from "presets";
import { changeNavVis } from "src/redux/actions";

class Search extends React.Component {
    constructor(props) {
        super(props);

        // Create a reference to input 
        this.input = React.createRef();

        this.state = {
            open: false,
            users: [],
            initial: true
        };

        this.interval = 600;

        // bind functions
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.sendXHR = this.sendXHR.bind(this);
        this.delaySend = this.delaySend.bind(this);
        this.handler = this.delaySend();
    }

    componentWillUnmount() {
        this.input.current.removeEventListener("input", this.handler);
        // Show the navbar
        this.props.changeVisibility(true);
    }

    open() {
        // get rid of body scrollability 
        const b = document.querySelector("body");
        b.style.overflow = "hidden";
        
        this.setState({ open: true });  
        // Hide the navbar
        this.props.changeVisibility(false);

        this.input.current.addEventListener("input", this.handler);
    }

    close() {
        // add body scrollability 
        const b = document.querySelector("body");
        b.style.overflow = "normal";

        this.setState({ open: false });

        // Show the navbar
        this.props.changeVisibility(true);
        this.input.current.value = "";
        this.input.current.removeEventListener("input", this.handler);
    }

    sendXHR(value) {
        const xhr = new XMLHttpRequest();
        this.setState({ spinner: true });

        xhr.onreadystatechange = () => {
            if (xhr.status == 200 & xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                this.setState({ users: response, spinner: false, initial: false });
            }
        }

        const body = JSON.stringify({ requested: value });

        xhr.open("POST", BACK_END + "/searchusers")
        xhr.send(body);
    }

    delaySend() {
        let timeout;
        let prValue = "";

        const handler = () => {
            clearTimeout(timeout);
            const curValue = this.input.current.value;

            if (prValue != "" && curValue == "") {
                this.setState({ users: [] });
                prValue = "";
                return;
            }

            if (prValue == curValue || curValue == "") {
                return;
            }

            timeout = setTimeout(() => {
                this.sendXHR(curValue);
                prValue = curValue;
            }, this.interval);
        }
        return handler.bind(this);
    }

    render() {
        const open = this.state.open;
        const state = this.state;

        return <div className={open ? styles.FullContainer : styles.Container}>
            <div className={styles.Top}>
                <img onClick={this.close} className={open ? styles.Back : styles.Hidden} src="/assets/icons/back.svg" />
                <div className={styles.Input}>
                    <input onFocus={this.open} ref={this.input} placeholder="Search for someone" type="text" maxLength="60" />
                    <button><img src="/assets/icons/search.svg" /></button>
                </div>
            </div>
            {open ? <Results initial={state.initial} spinner={state.spinner} users={state.users} /> : ""}
        </div>
    }
}

//Redux stuff
function mapDispatchToProps(dispatch) {
    return {
        changeVisibility: (show) => dispatch(changeNavVis(show))
    }
}
Search = connect(null, mapDispatchToProps)(Search);

export default Search;