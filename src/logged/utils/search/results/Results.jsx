import React from "react";
import PropTypes from "prop-types";

import Row from "./row/Row";
import styles from "./styles.scss";
import {Header1} from "utils/headers/Headers";
import Spinner from "utils/spinner/Spinner";


class Results extends React.Component {
    render() {
        const props = this.props;
        const spinner = props.spinner;

        const users = props.users || [];
        const rows = users.map((user) => {
            return <Row key={user.nickname} nickname={user.nickname} avatar_url={user.avatar_url} />
        });
        
        const no_rows = rows.length == 0;
        const initial = props.initial;

        return <div className={styles.Container}>
            {initial ? "" : <Header1 label={no_rows ? "No rows" : props.title || "Results:"} />}

            {spinner ? <div className={styles.SpinnerContainer}><Spinner cls={styles.Spinner}/></div> : ""}

            <div className={styles.Rows}>{rows}</div>
        </div>
    }
}

Results.propTypes = {
    spinner: PropTypes.bool,
    users: PropTypes.arrayOf(PropTypes.object),
    title: PropTypes.string
}

export default Results;