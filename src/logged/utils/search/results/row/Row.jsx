import React from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";

import { DUA } from "presets";
import styles from "./styles.scss";
import Spinner from "utils/spinner/Spinner";

class Row extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            redirect: false
        }

        // bind functions 
        this.onload = this.onload.bind(this);
        this.specific = this.specific.bind(this);
    }

    specific() {
        this.setState({ redirect: true });
    }

    onload() {
        this.setState({ loaded: true });
    }

    render() {
        const p = this.props;
        const loaded = this.state.loaded;

        if (this.state.redirect) {
            return <Redirect to={`/redirectusername/${p.nickname}`} />
        }

        return <div onClick={this.specific} className={styles.Container}>
            <div className={styles.Cover}></div>
            <div className={loaded ? styles.Border : styles.NoDisplay}>
                <div className={styles.Wrapper}>
                    {!loaded ? <Spinner cls={styles.Spinner} /> : ""}
                    <img onLoad={this.onload} src={p.avatar_url || DUA} />
                </div>
            </div>
            <p>{p.nickname}</p>
        </div>
    }
}

Row.propTypes = {
    avatar_url: PropTypes.string.isRequired,
    nickname: PropTypes.string.isRequired
}

export default Row;