import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import styles from "./styles.scss";

class Info extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            expand: true,
            tooltip: false
        };

        // value 2 means two icons are loaded, 1 means only one is loaded
        this.aloaded = 0;

        // Extract from props
        this.likepost = this.props.likepost;
        this.sharehandle = this.props.sharehandle;

        // Window handler
        this.handler = () => {
            this.setState({ tooltip: false });
        };

        // bind functions 
        this.iconLoaded = this.iconLoaded.bind(this);
        this.tooltip = this.tooltip.bind(this);
    }

    componentDidMount() {
        window.addEventListener("click", this.handler, true);
    }
    
    componentWillUnmount() {
        window.removeEventListener("click", this.handler, true);
    }

    iconLoaded() {
        this.aloaded += 1;
        if (this.aloaded == 2) {
            this.setState({ loaded: true });
        }
    }

    tooltip() {
        this.setState({ tooltip: true });
    }

    render() {
        // Nice number represantation
        const initial = this.props.likes;
        let remainder;
        let number;
        if (initial > 1000 * 1000) {
            const mls = Math.floor(initial / (1000 * 1000));
            remainder = initial - (1000 * 1000) * mls;
            const hrds = Math.floor(remainder / (100 * 1000));
            remainder = remainder - (100 * 1000) * hrds;
            const tens = Math.floor(remainder / (10 * 1000));
            remainder = remainder - (10 * 1000) * tens;
            const th = Math.floor(remainder / 1000);
            number = mls.toString();
            if (tens != 0 || hrds != 0 || mls != 0) {
                number += ".";
                number += hrds.toString();
                number += tens.toString();
                number += th.toString();
                number += "m";
            }
        } else if (initial > 1000) {
            const th = Math.floor(initial / 1000);
            remainder = initial - th * 1000;
            const hrds = Math.floor(remainder / 100);
            remainder = remainder - 100 * hrds;
            const tens = Math.floor(remainder / 10);
            number = th.toString();

            if (tens != 0 || hrds != 0) {
                number += ".";
                number += hrds.toString();
                number += tens.toString();
            }
            number += "k";
        } else {
            number = initial;
        }
        
        // Decide which heart icon to display 
        const liked = this.props.liked;
        const like_url = liked ? "/assets/icons/post/like.svg" : "/assets/icons/post/unlike.svg";

        // Get if logged from the context
        const logged = this.props.logged;


        // Which share icon to display
        const share_url = "/assets/icons/post/share.svg";

        const state = this.state;

        if (state.loaded) {
            return <div className={styles.Container}>
                <div className={state.tooltip ? styles.Tooltip : styles.Hidden}>
                    <p>You have to <Link to="/login">login</Link> to do it!</p>
                </div>

                <img onClick={logged ? this.likepost : this.tooltip} onLoad={this.iconLoaded} src={like_url} />
                <p className={styles.count}>{number}</p>
                <img onClick={logged ? this.sharehandle : this.tooltip} onLoad={this.iconLoaded} src={share_url} />
            </div>
        }
        return <div className={styles.HContainer}>
            <img onLoad={this.iconLoaded} src={like_url} />
            <p className={styles.count}>{number}</p>
            <img onClick={this.sharehandle} onLoad={this.iconLoaded} src={share_url} />
        </div>
    }
}

Info.defaultProps = {
    likepost: () => console.log("INFO CLICKED")
}

Info.propTypes = {
    shares: PropTypes.number,
    likes: PropTypes.number,
    logged: PropTypes.bool
}

export default Info;
