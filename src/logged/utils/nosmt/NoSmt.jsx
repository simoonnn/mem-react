import React from "react";

import styles from "./styles.scss";

class NoSmt extends React.Component {
    render() {
        return <p className={styles.Head}>{this.props.text}</p>
    }
}

export default NoSmt;