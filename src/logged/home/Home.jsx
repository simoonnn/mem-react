import React from "react";
 
import styles from "./styles.scss";
import Display from "../display/Display";
import Navbar from "utils/navbar/Navbar";
import {getCookie} from "utils/cookies/getCookie";
import {Redirect} from "react-router-dom";

class Home extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const nickname = getCookie("nickname");
        const token = getCookie("token");
        
        if (!nickname || !token) {
            return <Redirect to="/login"/>
        }

        return <div className={styles.Container}>
            <Display own={true} nickname={nickname} token={token}/>
            <Navbar redirect={false}/>
        </div>
    }
}

export default Home;