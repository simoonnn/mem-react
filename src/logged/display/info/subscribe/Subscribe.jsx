import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

import styles from "./styles.scss";
import { Cover } from "classes.scss";
import { BACK_END } from "presets";
import { getCookie } from "utils/cookies/getCookie";

class Subscribe extends React.Component {
    constructor(props) {
        super(props);

        // extract from props
        this.nickname = props.nickname;

        this.token = getCookie("token");
        this.ownnick = getCookie("nickname");
        this.ripple = null;

        // bind functions 
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.ripple = document.querySelector(`.${styles.Ripple}`);
    }

    handleClick() {
        this.ripple.classList.remove(styles.Active);
        setTimeout(() => {
            this.ripple.classList.add(styles.Active);
        }, 10);
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                if (xhr.response == "notlogged") {
                    this.props.history.push("/login");
                }
            }
        }
        xhr.open("POST", BACK_END + "/subscribe");
        // Construct body 
        const body = JSON.stringify({ nickname: this.ownnick, token: this.token, person: this.nickname });
        xhr.send(body);

        //Change the state of Info
        this.props.changeState();
    }

    render() {
        const label = this.props.label;

        return <div onClickCapture={this.handleClick} className={styles.Container}>
            <p className={styles.Ripple}></p>
            <button>{label}</button>
            <div className={Cover}></div>
        </div>
    }
}

Subscribe.propTypes = {
    nickname: PropTypes.string,
    token: PropTypes.string,
    label: PropTypes.string,
    changeState: PropTypes.func
}

export default withRouter(Subscribe);