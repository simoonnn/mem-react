import React from "react";
 
import styles from "./styles";

class More extends React.Component {
    render() {
        return <p className={styles.Link}>More</p>
    }
}

export default More;