import React from "react";

import styles from "./styles.scss";
import Number from "./number/Number";
import More from "./more/More"
import { SUBSCRIBERS, SUBSCRIPTIONS } from "presets";

class Properties extends React.Component {
    render() {
        const state = this.props.state;
        return <div className={styles.MainContainer}>
            <div className={styles.Container}>
                <Number title="Subscribers" type={SUBSCRIBERS} loading={state.loading} number={state.info.subscribers} />
                <Number title="Subscriptions" type={SUBSCRIPTIONS} loading={state.loading} number={state.info.subscriptions} />
            </div>
            <More />
        </div>
    }
}

export default Properties;