import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import SpinnerItself from "utils/spinner/Spinner";
import styles from "./styles.scss";
import { showInfo } from "src/redux/actions";

class Number extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false
        };

        //bind functions 
        this.open = this.open.bind(this);
    }

    open() {
        this.props.showInfo(this.props.type);
    }

    render() {
        // Nice number represantation
        const initial = this.props.number;
        let remainder;
        let number;
        if (initial > 1000 * 1000) {
            const mls = Math.floor(initial / (1000 * 1000));
            remainder = initial - (1000 * 1000) * mls;
            const hrds = Math.floor(remainder / (100 * 1000));
            remainder = remainder - (100 * 1000) * hrds;
            const tens = Math.floor(remainder / (10 * 1000));
            remainder = remainder - (10 * 1000) * tens;
            const th = Math.floor(remainder / 1000);
            number = mls.toString();
            if (tens != 0 || hrds != 0 || mls != 0) {
                number += ".";
                number += hrds.toString();
                number += tens.toString();
                number += th.toString();
                number += "m";
            }
        } else if (initial > 1000) {
            const th = Math.floor(initial / 1000);
            remainder = initial - th * 1000;
            const hrds = Math.floor(remainder / 100);
            remainder = remainder - 100 * hrds;
            const tens = Math.floor(remainder / 10);

            if (tens != 0 || hrds != 0) {
                number = th.toString();
                number += ".";
                number += hrds.toString();
                number += tens.toString();
                number += "k";
            }
        } else {
            number = initial;
        }

        const Spinner = <div className={styles.SpinnerContainer}>
            <SpinnerItself radius="25px" border="3px" />
        </div>
        return <div onClick={this.open} className={styles.Container}>
            <p className={styles.title}>{this.props.title}</p>
            {this.props.loading ? Spinner : <p className={styles.value}>{number}</p>}
        </div>
    }
}

Number.propTypes = {
    title: PropTypes.string,
    number: PropTypes.number.isRequired,
    type: PropTypes.string
}

function mapDispatchToProps(dispatch) {
    return {
        showInfo: (type) => dispatch(showInfo(type))
    }
}

Number = connect(null, mapDispatchToProps)(Number);

export default Number;