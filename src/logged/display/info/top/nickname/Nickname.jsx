import React from "react";
import PropTypes from "prop-types";

import styles from "./styles.scss";

class Nickname extends React.Component {
    render() {
        return <div className={styles.Container}>    
            <p>{this.props.nickname}</p>
        </div>
    }
}

Nickname.propTypes = {
    nickname: PropTypes.string
}

export default Nickname;
