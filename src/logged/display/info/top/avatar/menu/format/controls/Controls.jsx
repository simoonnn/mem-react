import React from "react";
import PT from "prop-types";

import styles from "./styles.scss";

const imageK = 0.05;
const selectorK = 0.05;

class Controls extends React.Component {
    render() {
        return <div className={styles.Container}>
            <div className={styles.Flex}>
                <div className={styles.ControlContainer}>
                    <span className={styles.label}>Frame</span>
                    <button onClick={this.props.resizeSelector(1 + selectorK)} className={styles.control}>&#43;</button>
                    <button disabled={this.props.selBlock} onClick={this.props.resizeSelector(1 - selectorK)} className={styles.control}>&#45;</button>
                </div>

                <div className={styles.ControlContainer}>
                    <button onClick={this.props.restoreImage} className={styles.button}>Restore</button>
                </div>

                <div className={styles.ControlContainer}>
                    <span className={styles.label}>Image</span>
                    <button disabled={this.props.maxBlock} onClick={this.props.resizeImage(1 + imageK)} className={styles.control}>&#43;</button>
                    <button disabled={this.props.minBlock} onClick={this.props.resizeImage(1 - imageK)} className={styles.control}>&#45;</button>
                </div>
            </div>
            <div className={styles.Resize}>
                <span className={styles.label}>Rotate</span>
                <button /* rotate clockwise */ onClick={this.props.rotateImage(90)} className={styles.control}>&#8635;</button>
                <button /* rotate anti-clockwise */ onClick={this.props.rotateImage(-90)} className={styles.control}>&#8634;</button>
            </div>
        </div>
    }
}

Controls.propTypes = {
    resizeImage: PT.func,
    resizeSelector: PT.func,
    restoreImage: PT.func,
    rotateImage: PT.func,
    maxBlock: PT.bool,
    minBlock: PT.bool,
    selBlock: PT.bool
}

export default Controls;