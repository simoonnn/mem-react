import React from "react";
import PT from "prop-types";
import classnames from "classnames";

import styles from "./styles.scss";
import Controls from "./controls/Controls";
import CoolSpinner from "./coolspinner/CoolSpinner";

const SIZE = 200;

const moveParts = 32;
let pxPerMove = 10;
const moveInterval = 50;

// max and min k for image
const minKImage = 0.7;
const maxKImage = 3;

// max selector size
const maxSSize = 50;

class Format extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            url: "",
            loaded: true,
            minBlock: false,
            maxBlock: false,
            selBlock: false
        }

        // initial image and selector k
        this.imageK = 1;
        this.selectorK = 1;
        this.degrees = 0;

        // moving horizontally and vertically intervals
        this.hMoving = false;
        this.vMoving = false;

        // utility 
        this.resizeImage1 = this.resizeImage(1);

        // bind functions
        this.touchStart = this.touchStart.bind(this);
        this.touchEnd = this.touchEnd.bind(this);
        this.touchMove = this.touchMove.bind(this);
        this.resizeImage = this.resizeImage.bind(this);
        this.resizeSelector = this.resizeSelector.bind(this);
        this.restoreImage = this.restoreImage.bind(this);
        this.rotateImage = this.rotateImage.bind(this);
        this.cutAndSend = this.cutAndSend.bind(this);
    }
    componentDidMount() {
        this.selector = document.getElementsByClassName(styles.Selector)[0];
        this.selector.addEventListener("touchstart", this.touchStart)

        this.selector.addEventListener("touchend", this.touchEnd)

        // the image 
        this.image = document.querySelector(`.${styles.image} img`);
        this.imageContainer = this.image.parentElement;

        // get the image data url
        const fr = new FileReader();
        fr.onload = (e) => {
            this.setState({ url: e.target.result });
        }
        fr.readAsDataURL(this.props.file);
    }

    componentWillUnmount() {
        // avoid memory leaks
        this.selector.removeEventListener("touchstart", this.touchStart)
        this.selector.removeEventListener("touchend", this.touchEnd)
        window.removeEventListener("touchmove", this.touchMove)
    }

    touchStart(e) {
        const style = window.getComputedStyle(this.selector);
        const top = parseFloat(style.top);
        const left = parseFloat(style.left);

        const touch = e.changedTouches[0];
        this.diffX = touch.clientX - left;
        this.diffY = touch.clientY - top;

        window.addEventListener("touchmove", this.touchMove)
    }

    touchEnd() {
        window.removeEventListener("touchmove", this.touchMove)
    }

    touchMove(e) {
        const touch = e.changedTouches[0]
        const cx = touch.clientX - this.diffX;
        const cy = touch.clientY - this.diffY;

        // check if the move is possible;
        let iRect = this.image.getBoundingClientRect();
        const sRect = this.selector.getBoundingClientRect();
        const bottomRightX = cx + sRect.width;
        const bottomRightY = cy + sRect.height;
        const topbottom = iRect.bottom >= bottomRightY && iRect.top <= cy;
        const leftright = iRect.left <= cx && iRect.right >= bottomRightX;

        // image container height and width
        const pRect = this.imageContainer.getBoundingClientRect();

        // if selector cannot reach the image
        if (cx < pRect.left && iRect.left < pRect.left ||
            pRect.right < iRect.right && bottomRightX > pRect.right ||
            cy < pRect.top && iRect.top < pRect.top ||
            bottomRightY > pRect.bottom && iRect.bottom > pRect.bottom) {
            let hCurrent = parseFloat(this.image.style.left) || 0;
            let vCurrent = parseFloat(this.image.style.top) || 0;
            switch (true) {
                case cx < pRect.left && iRect.left < cx:
                    if (this.hMoving) break;

                    this.selector.style.left = pRect.left + "px";
                    this.hMoving = setInterval(() => {
                        const iRect = this.image.getBoundingClientRect();
                        if (iRect.left >= pRect.left) {
                            clearInterval(this.hMoving);
                            this.hMoving = false;
                            return;
                        }
                        if (pxPerMove > Math.abs(iRect.left)) {
                            hCurrent += Math.abs(iRect.left);
                        } else {
                            hCurrent += pxPerMove;
                        }
                        this.image.style.left = hCurrent + "px";
                    }, moveInterval);
                    break;

                case bottomRightX < iRect.right && bottomRightX > pRect.right:
                    if (this.hMoving) break;

                    this.selector.style.left = pRect.right - sRect.width + "px";
                    this.hMoving = setInterval(() => {
                        const iRect = this.image.getBoundingClientRect();
                        if (iRect.right <= pRect.right) {
                            clearInterval(this.hMoving);
                            this.hMoving = false;
                            return;
                        }
                        const hDif = iRect.right - pRect.right;
                        if (pxPerMove > hDif) {
                            hCurrent -= hDif;
                        } else {
                            hCurrent -= pxPerMove;
                        }
                        this.image.style.left = hCurrent + "px";
                    }, moveInterval);
                    break;

                case cy > iRect.top && cy < pRect.top:
                    if (this.vMoving) break;

                    this.selector.style.top = pRect.top + "px";
                    this.vMoving = setInterval(() => {
                        const iRect = this.image.getBoundingClientRect();
                        if (iRect.top >= pRect.top) {
                            clearInterval(this.vMoving);
                            this.vMoving = false;
                            return;
                        }
                        const vDif = pRect.top - iRect.top;
                        if (pxPerMove > vDif) {
                            vCurrent += vDif;
                        } else {
                            vCurrent += pxPerMove;
                        }
                        this.image.style.top = vCurrent + "px";
                    }, moveInterval);
                    break;

                case bottomRightY < iRect.bottom && bottomRightY > pRect.bottom:
                    if (this.vMoving) break;

                    this.selector.style.top = pRect.bottom - sRect.height + "px";
                    this.vMoving = setInterval(() => {
                        const iRect = this.image.getBoundingClientRect();
                        if (iRect.bottom <= pRect.bottom) {
                            clearInterval(this.vMoving);
                            this.vMoving = false;
                            return;
                        }
                        const vDif = iRect.bottom - pRect.bottom;
                        if (pxPerMove > vDif) {
                            vCurrent -= vDif;
                        } else {
                            vCurrent -= pxPerMove;
                        }
                        this.image.style.top = vCurrent + "px";
                    }, moveInterval);
                    break;
            }
            return;
        } else if (leftright && topbottom) {
            this.selector.style.left = cx + "px";
            this.selector.style.top = cy + "px";
        } else {
            switch (true) {
                case leftright:
                    this.selector.style.left = cx + "px";
                    if (iRect.bottom > bottomRightY) {
                        // position on top
                        this.selector.style.top = iRect.top + "px";
                    } else {
                        this.selector.style.top = iRect.bottom - sRect.height + "px";
                    }
                    break;

                case topbottom:
                    // position on the left
                    this.selector.style.top = cy + "px";
                    if (iRect.right > bottomRightX) {
                        this.selector.style.left = iRect.left + "px";
                    } else {
                        this.selector.style.left = iRect.right - sRect.width + "px";
                    }
                    break;

                default:
                    if (iRect.right >= bottomRightX) {
                        this.selector.style.left = iRect.left + "px";
                    } else {
                        this.selector.style.left = iRect.right - sRect.width + "px";
                    }

                    if (iRect.bottom >= bottomRightY) {
                        // position on top
                        this.selector.style.top = iRect.top + "px";
                    } else {
                        this.selector.style.top = iRect.bottom - sRect.height + "px";
                    }
            }
        }

        if (this.hMoving) {
            clearInterval(this.hMoving);
            this.hMoving = false;
        }

        if (this.vMoving) {
            clearInterval(this.vMoving);
            this.vMoving = false;
        }
    }
    resizeImage(k) {
        return () => {
            this.imageK *= k;
            this.image.style.top = 0;
            this.image.style.left = 0;
            this.image.style.transform = `scale(${this.imageK}, ${this.imageK}) rotate(${this.degrees}deg)`;

            // tend to the selector
            let iRect = this.image.getBoundingClientRect();
            let sRect = this.selector.getBoundingClientRect();

            const iWidth = iRect.width;
            const iHeight = iRect.height;
            let sLength = sRect.width;

            if (iHeight < sLength || iWidth < sLength) {
                const length = Math.min(iHeight, iWidth);
                this.selector.style.height = length + "px";
                this.selector.style.width = length + "px";
                sLength = length;

                iRect = this.image.getBoundingClientRect();
            }

            if (iRect.left > sRect.left) {
                this.selector.style.left = iRect.left + "px";
            } else if (iRect.right < sRect.right) {
                this.selector.style.left = iRect.right - sLength + "px";
            }

            if (iRect.top > sRect.top) {
                this.selector.style.top = iRect.top + "px";
            } else if (iRect.bottom < sRect.bottom) {
                this.selector.style.top = iRect.bottom - sLength + "px";
            }

            // set the pxPerMove 
            pxPerMove = Math.min(iWidth, iHeight) / moveParts;

            // disable controls if needed
            if (this.imageK * k > maxKImage) {
                this.setState({ maxBlock: true });
                return;
            }
            if (this.imageK * k < minKImage) {
                this.setState({ minBlock: true });
                return;
            }

            // enable all buttons if no blocks
            if (!this.state.minBlock && !this.state.maxBlock) return;

            this.setState({ minBlock: false, maxBlock: false });
        }
    }

    resizeSelector(k) {
        return () => {
            const style = window.getComputedStyle(this.selector);
            const length = parseFloat(style.width)
            const iRect = this.image.getBoundingClientRect();
            const pRect = this.imageContainer.getBoundingClientRect();
            let newLength = length * k;

            // if greater than the image, don't do anything
            if (newLength > iRect.width || newLength > iRect.height ||
                newLength > pRect.width || newLength > pRect.height) {
                newLength = Math.min(iRect.width, iRect.height,
                    pRect.width, pRect.height);
            }

            if (newLength < maxSSize) {
                this.setState({ selBlock: true });
                return;
            }

            // move before resizing
            const top = parseFloat(style.top);
            const left = parseFloat(style.left);
            const newRight = left + newLength;
            const newBottom = top + newLength;

            const okBottom = iRect.bottom >= newBottom &&
                pRect.bottom >= newBottom;
            const okRight = iRect.right >= newRight &&
                pRect.right >= newRight;

            if (!okRight) {
                // issue with right border

                const diff = newRight - Math.min(iRect.right, pRect.right);
                this.selector.style.left = `${left - diff}px`;
            }
            if (!okBottom) {
                // issue with bottom border

                const diff = newBottom - Math.min(iRect.bottom,
                    pRect.bottom);
                this.selector.style.top = `${top - diff}px`;
            }

            this.selector.style.width = `${newLength}px`;
            this.selector.style.height = `${newLength}px`;

            // set selBlock to false if not false already
            if (this.state.selBlock) {
                this.setState({ selBlock: false });
            }
        }
    }

    restoreImage() {
        this.imageK = 1;
        this.degrees = 0;
        this.resizeImage1();
        this.rotateImage(0)();
    }

    rotateImage(deg) {
        return () => {
            this.degrees += deg;
            this.resizeImage1();
        }
    }

    cutAndSend() {
        const size = parseFloat(window.getComputedStyle(this.selector)["width"]);
        const canvas = document.createElement("canvas");
        canvas.setAttribute("width", SIZE + "px");
        canvas.setAttribute("height", SIZE + "px");

        if (canvas.getContext) {
            // determine the x, y of the image
            const iRect = this.image.getBoundingClientRect();
            const sRect = this.selector.getBoundingClientRect();
            // natural width and height
            const inWidth = this.image.naturalWidth;
            const inHeight = this.image.naturalHeight;

            // differences
            let xD = sRect.left - iRect.left;
            let yD = sRect.top - iRect.top;

            // in parts
            let xP;
            let yP;

            // selector size;
            let selSize;

            // depending on degrees, change the xP, yP, selSize
            const degRemainder = this.degrees % 360;
            switch (degRemainder) {
                case -270:
                case 90:
                    xP = yD / iRect.height;
                    yP = (iRect.right - sRect.right) / iRect.width;
                    selSize = size / iRect.height * inWidth;
                    break;
                case -180:
                case 180:
                    xP = (iRect.right - sRect.right) / iRect.width;
                    yP = (iRect.bottom - sRect.bottom) / iRect.height;
                    selSize = size / iRect.width * inWidth;
                    break;
                case -90:
                case 270:
                    xP = (iRect.bottom - sRect.bottom) / iRect.height;
                    yP = (sRect.left - iRect.left) / iRect.width;
                    selSize = size / iRect.height * inWidth;
                    break;
                default:
                    xP = xD / iRect.width;
                    yP = yD / iRect.height;
                    selSize = size / iRect.width * inWidth;
            }
            // in px
            const x = xP * inWidth;
            const y = yP * inHeight;

            const ctx = canvas.getContext("2d");

            // rotate the context
            const radians = this.degrees * Math.PI / 180;
            ctx.translate(SIZE / 2, SIZE / 2);
            ctx.rotate(radians);
            ctx.translate(-SIZE / 2, -SIZE / 2);

            ctx.drawImage(this.image, x, y, selSize, selSize, 0, 0, SIZE, SIZE);

            // This is what you want to store on the cloud
            const dataUrl = canvas.toDataURL("image/jpeg", 0.3);
            console.log(dataUrl);

            // close
            this.props.close();
        }
    }

    render() {
        const state = this.state;
        const url = state.url;
        const loaded = state.loaded;

        let imageClassname = styles.image;
        let selectorClassname = styles.Selector;
        if (!loaded) {
            imageClassname = classnames(styles.image, styles.hidden);
            selectorClassname = classnames(styles.Selector,
                styles.hidden);
        }

        return <div className={styles.Wrapper}>
            <div className={styles.Container}>
                <Controls resizeSelector={this.resizeSelector} resizeImage={this.resizeImage} restoreImage={this.restoreImage} rotateImage={this.rotateImage} maxBlock={state.maxBlock} minBlock={state.minBlock} selBlock={state.selBlock} />
                <div className={selectorClassname}></div>

                {!loaded ? <CoolSpinner /> : ""}

                <div className={imageClassname}>
                    <img onLoad={() => {
                        this.setState({ loaded: true });
                    }} src={url} />
                    <div className={styles.Cover}></div>
                </div>

                <div className={styles.buttons}>
                    <div className={styles.relative}>
                        <button>Select</button>
                        <div onClick={this.cutAndSend} className={styles.Cover}></div>
                    </div>

                    <div className={styles.relative}>
                        <button>Cancel</button>
                        <div onClick={this.props.close} className={styles.Cover}></div>
                    </div>
                </div>
            </div>
        </div>
    }
}

Format.propTypes = {
    file: PT.object,
    close: PT.func
}

export default Format;