import React from "react";

import styles from "./styles.scss";


class CoolSpinner extends React.Component {
    render() {
        return <div className={styles.Base}>
            <div className={styles.Loader}></div>
        </div>
    }
}

export default CoolSpinner;