import React from "react";
import PT from "prop-types";

import styles from "./styles.scss";
import Format from "./format/Format";
import { getCookie } from "utils/cookies/getCookie";
import { BACK_END } from "presets";
import { withRouter } from "react-router-dom";

class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            formatting: false,
            file: {}
        }

        // bind functions 
        this.deleteMessage = this.deleteMessage.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    deleteMessage(e) {
        // construct XHR
        const $this = this;
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.response == "notlogged") {
                    $this.props.history.push("/login");
                }
            }
        }

        xhr.open("POST", BACK_END + "/avatardelete");

        // construct body 
        const nickname = getCookie("nickname");
        const token = getCookie("token");
        const body = JSON.stringify({
            nickname,
            token
        });

        xhr.send(body);

        // actually change the avatar
        this.props.delete();
    }

    onChange(e) {
        const file = e.target.files[0];

        this.setState({ formatting: true, file });
    }

    render() {
        const deleteButton = this.props.deleteButton;
        const state = this.state;
        const formatting = state.formatting;

        if (formatting) {
            return <Format close={this.props.close} file={state.file} />
        }
        return <div onClick={this.props.close} className={styles.Wrapper}>
            <div onClick={(e) => e.stopPropagation()} className={styles.Container}>
                <label className={styles.control}>
                    <input onChange={this.onChange} type="file" accept="image/*" />
                    <p>Load a new avatar</p>
                </label>
                {deleteButton ?
                    <button onClick={this.deleteMessage} className={styles.control}>Delete current avatar</button>
                    : ""}
                <button onClick={this.props.close} className={styles.control}>Cancel</button>
            </div>
        </div>
    }
}

Menu.propTypes = {
    close: PT.func,
    delete: PT.func,
    deleteButton: PT.bool
}

Menu = withRouter(Menu);

export default Menu;