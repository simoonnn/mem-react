import React from "react";
import PropTypes from "prop-types";

import Spinner from "utils/spinner/Spinner";
import { DUA } from "presets";
import styles from "./styles.scss";
import { Cover } from "classes";
import Menu from "./menu/Menu";

class Avatar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            spinner: true,
            menuOpen: false,
            default: false
        }

        // Bind functions 
        this.imageLoaded = this.imageLoaded.bind(this);
        this.openMenu = this.openMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
        this.deleteAvatar = this.deleteAvatar.bind(this);
    }
    imageLoaded() {
        this.setState({ spinner: false });
    }

    openMenu() {
        this.setState({ menuOpen: true });

        // set overflow of the body to hidden
        const body = document.querySelector("body");
        body.style.overflow = "hidden";
    }

    closeMenu() {
        this.setState({ menuOpen: false });

        // set overflow of the body to visible
        const body = document.querySelector("body");
        body.style.overflow = "visible";
    }

    deleteAvatar() {
        this.setState({ default: true });
    }

    render() {
        let image;
        const open = this.state.menuOpen;
        const def = this.state.default;
        const own = this.props.own;

        if (this.props.url == "" && !this.props.loading || def) {
            image = <img onLoad={this.imageLoaded} src={DUA} />
        } else {
            image = <img onLoad={this.imageLoaded} src={this.props.url} />
        }

        const useSpinner = this.state.spinner || this.props.loading;

        // whether or not should display delete button in the Menu
        const should = !!this.props.url && !def;

        return <div className={useSpinner ? styles.SpinnerContainer : styles.Wrapper}>
            {open && own ? <Menu delete={this.deleteAvatar} close={this.closeMenu} deleteButton={should} /> : ""}
            {useSpinner ? <Spinner radius="60px" border="5px" /> : ""}


            <div className={styles.IContainer}>
                {image}
                <div onClick={own ? this.openMenu : () => { }} className={Cover}></div>
            </div>
        </div>

    }
}

Avatar.propTypes = {
    url: PropTypes.string.isRequired,
    own: PropTypes.bool
}

export default Avatar;