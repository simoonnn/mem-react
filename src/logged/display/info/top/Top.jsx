import React from "react";
import PT from "prop-types";

import styles from "./styles.scss";

import Nickname from "./nickname/Nickname";
import Avatar from "./avatar/Avatar";
import Settings from "./settings/Settings";

class Top extends React.Component {
    render() {
        const state = this.props.state;

        return <div className={styles.Container}>
            <Avatar own={this.props.own} loading={state.loading} url={state.info.avatar_url}/>
            <Nickname nickname={this.props.nickname}/>
            <Settings />
        </div>
    }
}

Top.propTypes = {
    state: PT.object, 
    own: PT.bool
}

export default Top;