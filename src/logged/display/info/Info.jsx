import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import styles from "./styles.scss";
import { BACK_END } from "presets";
import Top from "./top/Top";
import Properties from "./properties/Properties";
import Subscribe from "./subscribe/Subscribe";
import { getCookie } from "utils/cookies/getCookie";

class Info extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            info: {
                avatar_url: "",
                subscribers: 0,
                subscriptions: 0,
                status: "",
                subscribed: false
            },
            loading: true
        }

        // Bind functions
        this.changeState = this.changeState.bind(this);
    }
    componentDidMount() {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                this.setState({
                    info: JSON.parse(xhr.response),
                    loading: false
                });
            }
        }

        const current = getCookie("nickname");

        const body = {
            nickname: this.props.nickname,
            current
        }

        xhr.open("POST", BACK_END + "/getuserinfo");
        xhr.send(JSON.stringify(body));
    }

    changeState() {
        const si = this.state.info;
        const subs = si.subscribers;
        let new_subs
        if (si.subscribed) {
            new_subs = subs - 1;
        } else {
            new_subs = subs + 1;
        }
        this.setState({
            info: Object.assign({}, si, {
                subscribed: !si.subscribed,
                subscribers: new_subs
            })
        });
    }

    render() {
        // Whether to display Subscribe button
        const logged = this.props.logged;
        const own = this.props.own;
        this.display = !own && logged;

        const nickname = this.props.nickname;
        const subscribed = this.state.info.subscribed;

        return <div className={styles.Container}>
            <Top own={own} nickname={nickname} state={this.state} />
            {this.display ? <Subscribe token={this.props.token} label={subscribed ? "Unsubscribe" : "Subscribe"} changeState={this.changeState} nickname={nickname} /> : ""}
            <Properties state={this.state} />
        </div>
    }
}

Info.propTypes = {
    nickname: PropTypes.string,
    token: PropTypes.string,
    own: PropTypes.bool
}

// Wire up Redux and React

function mapStateToProps(state) {
    return {
        logged: state.logged,
        own: state.own
    }
}

Info = connect(mapStateToProps)(Info);


export default Info;