import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

import styles from "./styles.scss";
import Post from "./post/Post";
import NewPost from "./newpost/NewPost";
import { BACK_END, PPR, DSD } from "presets";
import Spinner from "utils/spinner/Spinner";
import NoSmt from "utils/nosmt/NoSmt";
import { getCookie } from "utils/cookies/getCookie";
import isLogged from "utils/logged/isLogged";

class Posts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: [],
            max: true,
            fetched: false
        }

        this.max = true;
        this.offset = 0;

        //Extract from props
        this.nickname = this.props.nickname;
        this.token = this.props.token;
        this.own = this.props.own;
        this.loggedNickname = getCookie("nickname");

        //bind functions
        this.addNewPost = this.addNewPost.bind(this);
        this.deletePost = this.deletePost.bind(this);
        this.updatePostsExcluding = this.updatePostsExcluding.bind(this);
        this.getPosts = this.getPosts.bind(this);
        this.loadMore = this.loadMore.bind(this);
        this.onWindowScroll = this.onWindowScroll.bind(this);
    }

    componentDidMount() {
        this.getPosts();

        this.container = document.querySelector(`.${styles.Container}`);

        window.addEventListener("scroll", this.onWindowScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.onWindowScroll);
    }

    onWindowScroll() {
        if (!this.state.max) {
            const scroll = window.scrollY;
            const vheight = window.innerHeight;
            // total is the total scroll
            const total = scroll + vheight;
    
            // compare with the height of the container
            const cheight = this.container.scrollHeight;

            if (cheight - total < DSD) {
                this.loadMore();
            }
        }
    }

    getPosts() {
        this.setState({ spinner: true, max: true });
        const xhr = new XMLHttpRequest();

        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                if (xhr.response == "") {
                    this.props.history.push("/login");
                    return;
                }
                const response = JSON.parse(xhr.response);
                const posts = this.state.posts;

                let max = false;
                if (response.length != PPR) {
                    max = true;
                }
                posts.push(...response);
                this.setState({ posts: posts, max, spinner: false, fetched: true });
            }
        }

        xhr.open("POST", BACK_END + "/getposts");
        xhr.send(JSON.stringify({ offset: this.offset, nickname: this.nickname, logged: this.loggedNickname }));
    }

    loadMore() {
        if (this.state.max) {
            return;
        }
        this.offset = this.state.posts.length;
        this.getPosts();
    }

    updatePostsExcluding(id) {
        let newPosts = this.state.posts;
        newPosts = newPosts.filter((val) => val.id != id);
        this.setState({ posts: newPosts });
    }

    deletePost(id) {
        const nickname = getCookie("nickname");
        const token = getCookie("token");

        return function () {
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.status == 200 & xhr.readyState == 4) {
                    if (xhr.response != "") {
                        const response = JSON.parse(xhr.response);
                        if (response.status == "success") {
                            this.updatePostsExcluding(id);
                        }
                    }
                }
            }

            xhr.open("POST", BACK_END + "/deletepost");
            xhr.send(JSON.stringify({ nickname, token, id }));
        }.bind(this);
    }


    addNewPost(post) {
        let posts = this.state.posts;
        posts.unshift(post);
        this.setState({ posts });
    }

    render() {
        let my_posts
        let logged = isLogged();

        if (this.state.fetched && this.state.posts.length == 0) {
            my_posts = <NoSmt text="No posts" />;
        } else if (this.own) {
            my_posts = this.state.posts.map((val) => {
                return <Post logged={logged} key={val.id} addPost={this.addNewPost} liked={logged ? val.liked : false} deletePost={this.deletePost} shared={logged ? val.shared : false} object={val} delPost={this.deletePost} />
            });
        } else {
            my_posts = this.state.posts.map((val) => {
                return <Post key={val.id} logged={logged} liked={logged ? val.liked : false} shared={logged ? val.shared : false} object={val} />
            });
        }

        const sEl = <div className={styles.SContainer}>
            <Spinner />
        </div>

        return <div className={styles.Container}>
            {this.own ? <NewPost post={this.addNewPost} /> : ""}
            <h1>Posts</h1>
            {my_posts}
            {this.state.spinner ? sEl : ""}
        </div>
    }
}

Posts.propTypes = {
    own: PropTypes.bool,
    token: PropTypes.string,
    nickname: PropTypes.string
}

// Wire up Redux and React
function mapStateToProps(state) {
    return {
        logged: state.logged,
        own: state.own
    }
}

Posts = connect(mapStateToProps)(Posts);
Posts = withRouter(Posts);
export default Posts;