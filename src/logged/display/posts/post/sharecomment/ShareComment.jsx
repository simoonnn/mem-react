import React from "react";
import PropTypes from "prop-types";

import styles from "./styles.scss";
import { maxLines } from "presets";

const delta = 50;

class ShareComment extends React.Component {
    constructor(props) {
        super(props);

        this.textarea = React.createRef();
        this.parent = React.createRef();

        //extract from props
        this.sharepost = props.sharepost;
        this.toggle = props.toggle;

        // ilineHeihgt and padding
        this.lineHeight = 0;
        this.padding = 0;

        //handler
        this.handler = () => {
            this.textarea.current.focus();
        };

        //bind functions
        this.close = this.close.bind(this);
        this.scroll = this.scroll.bind(this);
        this.sendandclose = this.sendandclose.bind(this);
        this.lineHandler = this.lineHandler.bind(this);
        this.setLineHeight = this.setLineHeight.bind(this);
    }

    lineHandler() {
        this.setLineHeight();
        this.scroll();
    }

    setLineHeight() {
        const styles = window.getComputedStyle(this.textarea.current);
        const lineHeight = parseFloat(styles["lineHeight"]);
        const padding = parseFloat(styles["paddingTop"]) + parseFloat(styles["paddingBottom"]);
        const border = parseFloat(styles["borderWidth"]) * 2;
        this.lineHeight = lineHeight;
        this.padding = padding + border;        
    }

    componentDidMount() {
        this.setLineHeight();
        window.addEventListener("resize", this.lineHandler);
    }

    componentWillUnmount() {
        const parent = this.parent.current;
        const handler = this.handler;

        // remove event listeners
        parent.removeEventListener("webkitAnimationEnd", handler);
        parent.removeEventListener("animationend", handler);
        window.removeEventListener("resize", this.lineHandler);
    }

    componentDidUpdate() {
        if (this.props.show) {
            const offsetY = this.parent.current.getBoundingClientRect().top;
            const offset = offsetY - delta;
            window.scrollBy(0, offset);

            const parent = this.parent.current;
            const handler = this.handler;

            parent.addEventListener("webkitAnimationEnd", handler);
            parent.addEventListener("animationend", handler);
        }
    }

    scroll() {
        this.textarea.current.style.height = "0px";
        let linesNum = Math.ceil((this.textarea.current.scrollHeight - this.padding) / this.lineHeight);
        if (linesNum > maxLines) {
            linesNum = maxLines;
        }
        this.textarea.current.style.height = `${linesNum * this.lineHeight}px`;
    }

    sendandclose() {
        this.props.toggle();

        // Get the value 
        const value = this.textarea.current.value;
        this.sharepost(value);

        // Update textarea's value 
        this.textarea.current.value = "";
    }

    close() {
        this.toggle();

        // Update textarea's value 
        this.textarea.current.value = "";
    }

    render() {
        const show = this.props.show;
        const cls = show ? styles.Expanded : styles.Hidden;

        return <div className={cls} ref={this.parent}>
            <h1>Sharing post</h1>
            <h2>Add description or whatever you want</h2>

            <textarea onInput={this.scroll} maxLength={1300} ref={this.textarea}></textarea>

            <div className={styles.ButtonsContainer}>
                <button onClick={this.close}>cancel</button>
                <button onClick={this.sendandclose} className={styles.Commit}>commit</button>
            </div>
        </div>
    }
}

ShareComment.propTypes = {
    toggle: PropTypes.func.isRequired,
    sharepost: PropTypes.func.isRequired
}

export default ShareComment;