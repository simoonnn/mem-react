import React from "react";


import styles from "./styles.scss";


class PostMenu extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            show: false
        };

        // Assing props and context to variables
        this.deletePost = props.deletePost;
        this.id = this.props.id;

        //Bind handlers
        this.showHide = this.showHide.bind(this);

        //window event listener
        this.wListener = () => {
            this.setState({show: false});
        }
    }
    componentWillUnmount() {
        window.removeEventListener("scroll", this.wListener)
        window.removeEventListener("click", this.wListener)
    }
    componentDidMount() {
        window.addEventListener("scroll", this.wListener);
        window.addEventListener("click", this.wListener);
    }

    render() {
        return <div className={styles.Container}>
            <img src="assets/icons/postMenu.svg" onClick={this.showHide}/>
            <div className={this.state.show ? styles.Show : styles.Hidden}>
                <button>Edit</button>
                <button onClick={this.deletePost(this.id)}>Delete</button>
            </div>
        </div>
    }

    showHide(e) {
        e.stopPropagation();
        this.setState({show: !this.state.show});
    }
}

export default PostMenu;