import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import styles from "./styles.scss";
import Info from "utils/info/Info";
import PostMenu from "./menu/PostMenu";
import { getCookie } from "utils/cookies/getCookie";
import { BACK_END } from "presets";
import ShareComment from "./sharecomment/ShareComment";

class Post extends React.Component {
    constructor(props) {
        super(props);
        this.po = props.object;
        this.state = {
            likes: this.po.likes,
            liked: this.po.liked,
            expand: false
        };

        // XHR is in progress
        this.loadingShares = false;
        this.loadingLikes = false;

        // extract from props
        this.changeState = props.changeState;
        this.addNewPost = props.addPost;

        // bind functions
        this.likepost = this.likepost.bind(this);
        this.sharepost = this.sharepost.bind(this);
        this.toggle = this.toggle.bind(this);
    }
    
    likepost() {
        const prevState = this.state;
        
        // Update state
        const likes_dif = prevState.liked ? -1 : 1;
        const new_likes = prevState.likes + likes_dif;
        this.setState({ likes: new_likes, liked: !prevState.liked });
        
        const nickname = getCookie("nickname");
        const token = getCookie("token");
        
        if (nickname && !this.loadingLikes) {
            const xhr = new XMLHttpRequest();
            this.loadingLikes = true;
            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    status = xhr.response;
                    if (status == "error") {
                        alert("There was an error! The like may not be legit(");
                    }
                } else if (xhr.readyStatus == 4) {
                    this.loadingLikes = false;
                }
            }

            const body = JSON.stringify({ nickname, token, post_id: this.props.object.id });

            xhr.open("POST", BACK_END + "/likepost");
            xhr.send(body);
        }
    }

    toggle() {
        this.setState({ expand: !this.state.expand });
    }

    sharepost(value) {
        const nickname = getCookie("nickname");
        const token = getCookie("token");

        if (this.props.logged && !this.loadingShares) {

            // get the epoch time
            const date = new Date();
            this.epoch = date.valueOf();

            const xhr = new XMLHttpRequest();
            this.loadingShares = true;
            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    const response = JSON.parse(xhr.response);
                    const status = response.status;
                    if (status == "error") {
                        alert("There was an error! The like may not be legit(");
                    }
                    if (status == "success" && this.context) {
                        const po = this.po;

                        const new_post = {
                            contents: po.contents,
                            author: po.author,
                            image: po.image,
                            likes: 0,
                            liked: false,
                            owner: po.owner,
                            description: value,
                            id: response.id,
                            date: this.epoch
                        };

                        if (this.props.own) {
                            this.addNewPost(new_post);
                        }
                    }
                } else if (xhr.readyStatus == 4) {
                    this.loadingShares = false;
                }
            }

            const body = JSON.stringify({
                nickname,
                token,
                post_id: this.props.object.id,
                description: value
            });

            xhr.open("POST", BACK_END + "/sharepost");
            xhr.send(body);
        }
    }

    render() {
        const props = this.props;
        const po = this.po;

        // construct a cool date
        const now = new Date().valueOf();
        const epoch = po.date;
        const date = new Date(epoch);
        let displayed;

        // decide what the locale should be 
        const locale = navigator.userLanguage || navigator.browserLanguage || navigator.languages[0] || "en";

        // one day in milliseconds
        const oneDay = 1000 * 3600 * 24;
        const oneYear = oneDay * 365;

        if (now - epoch >= oneYear) {
            displayed = date.toLocaleDateString(locale, {
                month: "long",
                day: "numeric",
                year: "numeric"
            });
        } else if (now - epoch >= oneDay) {
            displayed = date.toLocaleDateString(locale, {
                month: "long",
                day: "numeric"
            });
        } else {
            displayed = date.toLocaleTimeString(locale, {
                hour: "2-digit",
                minute: "2-digit"
            });
        }

        // composes images
        let images;
        images = po.images.map((img) => {
            return <img key={img.name} src={img.src} />
        })

        const state = this.state;
        return <div ref={this.container} className={styles.Container}>
            <ShareComment sharepost={this.sharepost} toggle={this.toggle} show={state.expand} />
            {this.props.own && !state.expand ? <PostMenu deletePost={props.deletePost} id={po.id} /> : ""}
            {po.description ? <p className={styles.desc}>{po.description}</p> : ""}
            <p className={styles.text}>{po.contents}</p>
            {images.length > 0 ? <div className={styles.images}>
                {images}
            </div> : ""}

            <div className={styles.DateContainer}>
                <p className={styles.date}>{displayed}</p>
                <p className={styles.author}>{po.author}</p>
            </div>

            <Info likes={state.likes} logged={this.props.logged} liked={state.liked} likepost={this.likepost} sharehandle={this.toggle} />
        </div>
    }
}

Post.propTypes = {
    object: PropTypes.object.isRequired,
    liked: PropTypes.bool.isRequired
}
function mapStateToProps(state) {
    return {
        own: state.own
    }
}

Post = connect(mapStateToProps)(Post);

export default Post;