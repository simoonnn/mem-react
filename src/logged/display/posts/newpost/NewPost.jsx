import React from "react";


import Controls from "./controls/Controls";
import styles from "./styles.scss";
import { getCookie } from "utils/cookies/getCookie";
import { BACK_END, maxLines } from "presets";

class NewPost extends React.Component {
    constructor(props) {
        super(props);

        this.textarea = React.createRef();

        // bind functions 
        this.scroll = this.scroll.bind(this);
        this.newPost = this.newPost.bind(this);
        this.setLineHeight = this.setLineHeight.bind(this);
        this.handler = this.handler.bind(this);
    }

    setLineHeight() {
        const styles = window.getComputedStyle(this.textarea.current);
        const lineHeight = parseFloat(styles["lineHeight"]);
        const padding = parseFloat(styles["paddingBottom"]) + parseFloat(styles["paddingTop"]);
        this.lineHeight = lineHeight;
        this.padding = padding;
    }

    handler() {
        this.setLineHeight();
        this.scroll();
    }    

    componentDidMount() {
        window.addEventListener("resize", this.handler);
        this.setLineHeight();
        this.textarea.current.height = `${this.lineHeight}px`;
    }

    componentWillUnmount() {
        // Remove event listeners
        window.removeEventListener("resize", this.handler);
    }

    scroll() {
        this.textarea.current.style.height = "0px";
        const scrollHeight = this.textarea.current.scrollHeight - this.padding;
        let linesNum = Math.ceil(scrollHeight / this.lineHeight);
        if (linesNum > maxLines) {
            linesNum = maxLines;
        }
        this.textarea.current.style.height = `${linesNum * this.lineHeight}px`;
    }

    newPost(images) {
        const taValue = this.textarea.current.value.trim();

        if (taValue == "" && images.length < 1) {
            return;
        }
        const nickname = getCookie("nickname");
        const token = getCookie("token");

        // create date 
        const date = new Date();
        this.epoch = date.valueOf();

        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 & xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                const id = response.id;
                const status = response.status;
                if (status != "success") {
                    alert("Something went wrong! Please try again!");
                    return;
                }

                const post = {
                    date: this.epoch,
                    author: nickname,
                    contents: taValue,
                    likes: 0,
                    shares: 0,
                    images,
                    id,
                    liked: false
                };
                this.props.post(post);
            }
        }

        const body = {
            contents: taValue,
            owner: nickname,
            token
        };

        xhr.open("POST", BACK_END + "/newpost");
        xhr.send(JSON.stringify(body));

        this.textarea.current.value = "";
        this.textarea.current.style.height = `${this.lineHeight}px`;
    }

    render() {
        return <div className={styles.Container}>
            <textarea onInput={this.scroll} maxLength={1200} ref={this.textarea} className={styles.textarea}></textarea>
            <Controls post={this.newPost} />
        </div>
    }
}

export default NewPost;