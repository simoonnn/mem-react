import React from "react";

import Images from "./images/Images";
import styles from "./styles.scss";


class Controls extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            images: []
        }

        // input reference 
        this.input = React.createRef();

        // bind functions 
        this.onChange = this.onChange.bind(this);
        this.closeImage = this.closeImage.bind(this);
        this.onPost = this.onPost.bind(this);
    }

    onChange(e) {
        const files = e.target.files;
        const images = this.state.images;

        for (let i = 0; i < files.length; i++) {
            // current file
            const cFile = files[i];
            const fName = cFile.name;

            let shouldStop = false;
            for (let i = 0; i < images.length; i++) {
                if (images[i].name == fName) {
                    shouldStop = true;
                    break;
                };
            }
            if (shouldStop) continue;

            // read the file
            const fr = new FileReader();
            fr.onload = (e) => {
                const images = this.state.images;
                images.push({ name: fName, src: e.target.result });
                this.setState({ images });
            }

            fr.readAsDataURL(cFile);
        }
    }

    closeImage(name) {
        return () => {
            // delete the image, update the state
            let images = this.state.images;
            images = images.filter((img) => {
                if (img.name == name) {
                    return false;
                }

                return true;
            });

            this.setState({ images });
        }
    }

    onPost() {
        this.props.post(this.state.images);
        this.setState({ images: [] });

        // clear files of images input
        this.input.current.value = "";
    }

    render() {
        const renderImages = this.state.images.length != 0;

        return <div className={styles.Container}>
            {renderImages ? <Images images={this.state.images} closeImage={this.closeImage} /> : ""}
            <div className={styles.Grid}>
                <label className={styles.attach}>
                    <img src="/assets/icons/attach.svg" />
                    <input ref={this.input} onChange={this.onChange} className={styles.image} type="file"
                        multiple accept="image/jpeg, image/png" />
                </label>
                <button className={styles.post} onClick={this.onPost} >Post</button>
            </div>
        </div>
    }
}

export default Controls;