import React from "react";
import PT from "prop-types";

import styles from "./styles.scss";

class Images extends React.Component {
    render() {
        const images = this.props.images.map((obj) => {
            return <label key={obj.name} className={styles.image}>
                <button onClick={this.props.closeImage(obj.name)} className={styles.button}></button>
                <img src={obj.src} />
            </label>
        })
        return <div className={styles.Container}>
            {images}
        </div>
    }
}

Images.propTypes = {
    images: PT.array,
    closeImage: PT.func
}

export default Images;