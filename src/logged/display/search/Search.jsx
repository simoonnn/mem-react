import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";


import styles from "utils/search/styles.scss";
import Results from "utils/search/results/Results";
import { BACK_END, SUBSCRIPTIONS, SUBSCRIBERS } from "presets";
import { changeNavVis } from "src/redux/actions";

class Search extends React.Component {
    constructor(props) {
        super(props);

        // Create a reference to input 
        this.input = React.createRef();

        this.state = {
            open: false,
            users: [],
            initial: true,
            spinner: true
        };

        this.interval = 600;
        this.timeout = null;
        this.offset = 0;

        // extract from props
        this.close = this.props.hide;
        this.type = this.props.type;

        // Decide on the title 
        if (this.type == SUBSCRIBERS) {
            this.title = "Subscribers:";
        } else if (this.type == SUBSCRIPTIONS) {
            this.title = "Subscriptions:";
        }

        // bind functions
        this.handler = this.delaySend();
    }

    componentWillUnmount() {
        // add body scrollability 
        const b = document.querySelector("body");
        b.style.overflow = "scroll";

        clearTimeout(this.timeout);
        // Show the navbar
        this.props.changeVisibility(true);
    }

    componentDidMount() {
        // get rid of body scrollability 
        const b = document.querySelector("body");
        b.style.overflow = "hidden";

        // Hide the navbar
        this.props.changeVisibility(false);

        const pr = this.props;
        const xhr = new XMLHttpRequest();

        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                const users = this.state.users;
                users.push(...response);
                this.setState({ users: users, initial: false, spinner: false });
            }
        }

        xhr.open("POST", BACK_END + "/getsubscriptions");

        // Construct body 
        const body = JSON.stringify({ type: pr.type, offset: this.offset, person: pr.nickname });
        xhr.send(body);
    }

    sendXHR(value) {
        const pr = this.props;
        const xhr = new XMLHttpRequest();
        this.setState({ spinner: true });

        xhr.onreadystatechange = () => {
            if (xhr.status == 200 & xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                this.setState({ users: response, spinner: false, initial: false });
            }
        }

        const body = JSON.stringify({ requested: value, type: pr.type, person: pr.nickname });

        xhr.open("POST", BACK_END + "/searchsubscriptions");
        xhr.send(body);
    }

    delaySend() {
        let prValue = "";
        const handler = () => {
            clearTimeout(this.timeout);
            const curValue = this.input.current.value;

            if (prValue != "" && curValue == "") {
                this.setState({ users: [] });
                prValue = "";
                return;
            }

            if (prValue == curValue || curValue == "") {
                return;
            }

            this.timeout = setTimeout(() => {
                this.sendXHR(curValue);
                prValue = curValue;
            }, this.interval);
        }
        return handler.bind(this);
    }

    render() {
        const state = this.state;
        return <div className={styles.FullContainer}>
            <div className={styles.Top}>
                <img onClick={this.close} className={styles.Back} src="/assets/icons/back.svg" />
                <div className={styles.Input}>
                    <input onInput={this.handler} ref={this.input} placeholder={this.props.title} type="text" maxLength="60" />
                    <button><img src="/assets/icons/search.svg" /></button>
                </div>
            </div>
            <Results initial={state.initial} title={this.title} spinner={state.spinner} users={state.users} />
        </div>
    }
}

Search.propTypes = {
    title: PropTypes.string,
    hide: PropTypes.func,
    type: PropTypes.string
}

//Redux stuff
function mapDispatchToProps(dispatch) {
    return {
        changeVisibility: (show) => dispatch(changeNavVis(show))
    }
}
Search = connect(null, mapDispatchToProps)(Search);

export default Search;