import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { BACK_END, SUBSCRIBERS, SUBSCRIPTIONS } from "presets";
import Spinner from "utils/spinner/Spinner-container";
import styles from "./styles.scss";
import Info from "./info/Info";
import Posts from "./posts/Posts";
import NotAvailable from "./notavailable/NotAvailable";
import Search from "./search/Search";
import SearchPeople from "utils/search/Search";

//Redux
import { getCookie } from "utils/cookies/getCookie";
import { changeNickname, changeOwn, hideInfo, changeLoggedIn } from "src/redux/actions";


class Display extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            exists: false
        }

        this.nickname = this.props.nickname;
        this.own = this.props.own;

        // create reference
        this.container = React.createRef();

        // Is the user logged in
        let loggedIn = false;
        this.token = getCookie("token");
        this.currentUser = getCookie("nickname");
        if (this.currentUser && this.token) {
            loggedIn = true;
        }

        // Change redux nickname value
        props.changeLoggedIn(loggedIn);
        props.changeNickname(this.nickname);
        props.changeOwn(this.own);
        props.hideInfo();
    }

    componentDidMount() {
        // add body scrollability 
        const b = document.querySelector("body");
        b.style.overflow = "normal";
        
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                this.setState({ loaded: true, exists: response.exists });
            }
        }

        const body = JSON.stringify({ nickname: this.nickname });

        xhr.open("POST", BACK_END + "/userexists");
        xhr.send(body);

    }
    
    componentDidUpdate() {
        // change paddingBottom
        if (this.container.current) {
            this.container.current.style.paddingBottom = `${this.props.navbarHeight}px`;
        }
    }

    render() {
        const state = this.state;
        const showInfo = this.props.show;
        const showType = this.props.type;

        // Choose a fitting title
        let title;
        switch (showType) {
            case SUBSCRIBERS:
                title = "Search subscribers";
                break
            case SUBSCRIPTIONS:
                title = "Search subscriptions";
                break
            default:
                title = undefined;
        }

        if (state.loaded && state.exists) {
            return <React.Fragment>
                {!showInfo ? <SearchPeople /> : ""}
                {showInfo ? <Search nickname={this.props.nickname} hide={this.props.hideInfo} type={showType} title={title} /> : ""}
                <div ref={this.container} className={styles.Container}>
                    <Info nickname={this.nickname} token={this.token} />
                    <Posts nickname={this.nickname} token={this.token} />
                </div>
            </React.Fragment>
        } else if (state.loaded) {
            return <NotAvailable />
        }

        return <Spinner />
    }
}

Display.propTypes = {
    own: PropTypes.bool,
    token: PropTypes.string,
    nickname: PropTypes.string
}

//Redux stuff
function mapDispatchToProps(dispatch) {
    return {
        changeNickname: (nickname) => dispatch(changeNickname(nickname)),
        changeLoggedIn: (isLoggedIn) => dispatch(changeLoggedIn(isLoggedIn)),
        changeOwn: (isOwn) => dispatch(changeOwn(isOwn)),
        hideInfo: () => dispatch(hideInfo())
    }
}

function mapStateToProps(state) {
    return {
        show: state.info.showInfo,
        type: state.info.type,
        navbarHeight: state.navbarHeight
    }
}

Display = connect(mapStateToProps, mapDispatchToProps)(Display);

export default Display;