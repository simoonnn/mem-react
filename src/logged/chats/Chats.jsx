import React from "react";
import { connect } from "react-redux";
import { withRouter, HashRouter } from "react-router-dom";

import styles from "./styles.scss";
import Choose from "./choose/Choose";
import { BACK_END } from "presets";
import { getCookie } from "utils/cookies/getCookie";
import Single from "./single/Single";
import Search from "./search/Search";
import BigSpinner from "utils/spinner/Spinner-container";
import Spinner from "utils/spinner/Spinner";
import { Header1 } from "utils/headers/Headers";
import Navbar from "utils/navbar/Navbar";

const CHECKINTERVAL = 2000;

class Chats extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            chats: [],
            offset: 0,
            loaded: false
        };

        this.nickname = getCookie("nickname");
        this.token = getCookie("token");

        // initial chats
        this.initialChats = [];

        // bind functions 
        this.openChat = this.openChat.bind(this);
        this.close = this.close.bind(this);
        this.setOffset = this.setOffset.bind(this);
        this.setChats = this.setChats.bind(this);
        this.returnInitialChats = this.returnInitialChats.bind(this);
        this.toggleSpinner = this.toggleSpinner.bind(this);
        this.checkChats = this.checkChats.bind(this);
        this.dealWithChats = this.dealWithChats.bind(this);
    }

    componentDidMount() {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                if (xhr.response == "notlogged") {
                    this.props.history.push("login")
                } else {
                    const response = JSON.parse(xhr.response);
                    this.initialChats = response;
                    this.setState({ chats: response, loaded: true });
                }
            }
        }

        // construct body 
        const body = JSON.stringify({
            nickname: this.nickname,
            token: this.token
        });

        xhr.open("POST", BACK_END + "/getchats");
        xhr.send(body);

        // Scroll the body 
        window.scrollTo(0, 0);

        // set the checking interval
        this.checkingTimeout = setTimeout(this.checkChats, CHECKINTERVAL);
    }

    componentWillUnmount() {
        clearTimeout(this.checkingTimeout);
    }

    dealWithChats(chats) {
        const currentChats = this.state.chats;
        chats.forEach((chat) => {
            const i = currentChats.findIndex((c) => {
                return c.chat_id == chat.chat_id;
            })

            currentChats[i] = chat;
        })

        this.setState({ chats: currentChats });
    }

    checkChats() {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                if (response.status == "success") {
                    // check again after some time
                    setTimeout(this.checkChats, CHECKINTERVAL);

                    const chats = response.chats;
                    if (chats.length == 0) return;
                    this.dealWithChats(chats);
                } else if (response.status == "notlogged") {
                    this.props.history.push("login");
                }
            }
        }

        xhr.open("POST", BACK_END + "/checkforchats");

        // construct the body 
        const body = JSON.stringify({
            token: this.token,
            nickname: this.nickname
        });
        xhr.send(body);
    }

    setOffset(offset) {
        this.setState({ offset });
    }

    openChat() {
        this.setState({ open: true });

        // make the body unscrollable
        const body = document.querySelector("body");
        body.style.overflow = "hidden";
    }

    close() {
        this.setState({ open: false })

        // make the body scrollable again
        const body = document.querySelector("body");
        body.style.overflow = "visible";
    }

    setChats(chats) {
        this.setState({ chats, loaded: true });
    }

    toggleSpinner(show) {
        this.setState({ loaded: !show });
    }

    returnInitialChats() {
        this.setState({ chats: this.initialChats });
    }

    render() {
        const choosing = this.state.open;

        // o.nickname, o.contents, o.date
        let chats = this.state.chats.sort((a, b) => {
            return b.date - a.date;
        })
        chats = chats.map((o) => {
            return <Single key={o.nickname} chat={o} />
        });

        // Set offset
        const offset = this.state.offset;
        const style = {
            top: offset,
            paddingBottom: this.props.padding
        }
        // decide whether to show the big spinner
        const bigSpinner = (offset == 0);

        // decide whether to show the small spinner
        const smallSpinner = !this.state.loaded;

        return <React.Fragment>
            <Choose hide={this.close} show={choosing} />
            {bigSpinner ? <BigSpinner /> : ""}
            <Search toggleSpinner={this.toggleSpinner} returnInitial={this.returnInitialChats} setChats={this.setChats} setOffset={this.setOffset} />
            <div style={style} className={styles.Container}>
                <Header1 label="Chats" />
                {smallSpinner ? <Spinner cls={styles.Spinner} /> :
                    this.state.chats.length > 0 ? <div>{chats}</div> : <h1 className={styles.NoChats}>
                        No chats
                    </h1>}
                <img className={styles.button} onClick={this.openChat} alt="Text your friend" src="/assets/icons/messages/writing.svg" />
            </div>
            <Navbar />
        </React.Fragment>
    }
}

// Redux stuff
function mapStateToProps(state) {
    return {
        padding: state.navbarHeight
    }
}

Chats = connect(mapStateToProps)(Chats);
Chats = withRouter(Chats);

export default Chats;