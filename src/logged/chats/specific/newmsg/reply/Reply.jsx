import React from "react";
import PT from "prop-types";
import classnames from "classnames";

import { replyLength } from "presets";
import styles from "./styles.scss";


class Reply extends React.Component {
    render() {
        const props = this.props;
        const author = props.author;
        let contents = props.contents;

        // trim contents
        if (replyLength < contents.length) {
            contents = contents.substring(0, replyLength);
            contents += "...";
        }

        return <div className={styles.Container}>
            <p className={styles.author}>{author}</p>
            <p className={styles.text}>
                {contents}
            </p>
        </div>
    }
}

Reply.propTypes = {
    author: PT.string.isRequired,
    contents: PT.string.isRequired
}

export default Reply;