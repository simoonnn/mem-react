import React from "react";
import PropTypes from "prop-types";

import styles from "./styles.scss";
import Reply from "./reply/Reply";
import { BACK_END } from "presets";
import { Cover } from "classes";
import { getCookie } from "utils/cookies/getCookie";
import generateToken from "utils/token/generate";

// some constants
const maxLines = 6;
const tokenLength = 15;

class New extends React.Component {
    constructor(props) {
        super(props);

        this.selfContainer = React.createRef();

        // extract from props 
        this.container = this.props.container;
        this.msgcontainer = this.props.msgcontainer;
        this.textarea = props.textarea;

        // initial lineHeight
        this.lineHeight = 0;

        // bind functions 
        this.scroll = this.scroll.bind(this);
        this.send = this.send.bind(this);
        this.reply = this.reply.bind(this);
        this.edit = this.edit.bind(this);
        this.push = props.pushMsg;
        this.markDelivered = props.markDelivered;
        this.scrollWW = this.scrollWW.bind(this);
        this.setLineHeight = this.setLineHeight.bind(this);
        this.setBorderRadius = this.setBorderRadius.bind(this);
        this.handler = this.handler.bind(this);
    }

    componentDidUpdate(prevProps) {
        // scroll only once when needed
        if (this.props.ready && !this.scrolled) {
            this.scrollWW();
            this.scrolled = true;
        }

        // scroll if editting or replying
        if (this.props.editting || this.props.replying || (!this.props.editting && prevProps.editting) || (!this.props.replying && prevProps.replying)) {
            this.scroll();
        }
    }

    componentDidMount() {
        this.setLineHeight();
        this.setBorderRadius();
        window.addEventListener("resize", this.handler);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handler);
    }

    handler() {
        this.setLineHeight();
        this.setBorderRadius();
        this.scroll();
    }

    setBorderRadius() {
        const textContainer = document.getElementsByClassName(styles.Textarea)[0];
        const style = window.getComputedStyle(textContainer);
        const padding = parseFloat(style["paddingTop"])
            + parseFloat(style["paddingBottom"]);
        const bRadius = 0.5 * (padding + this.lineHeight);
        textContainer.style.borderRadius = `${bRadius}px`;
    }

    setLineHeight() {
        const styles = window.getComputedStyle(this.textarea.current);
        const lineHeight = parseFloat(styles["lineHeight"]);
        const padding = parseFloat(styles["paddingTop"]) + parseFloat(styles["paddingBottom"]);
        const border = parseFloat(styles["borderWidth"]) * 2;
        this.lineHeight = lineHeight;
        this.padding = padding + border;
    }

    scroll() {
        const tarea = this.textarea.current;

        // This is used to get the correct scrollHeight
        tarea.style.height = "0px";
        let linesNum = Math.ceil(tarea.scrollHeight / this.lineHeight);
        if (linesNum > maxLines) {
            linesNum = maxLines;
        }

        const height = `${linesNum * this.lineHeight}px`;
        tarea.style.height = height;

        // Update grid-template-rows of Specific container
        const row = this.selfContainer.current.scrollHeight;
        this.container.current.style.paddingBottom = `${row}px`;
    }

    // scrolling with window
    scrollWW() {
        this.scroll();

        // Scroll window
        this.props.scroll();
    }

    // send messages
    send(e) {
        e.preventDefault();
        const value = this.textarea.current.value.trim();
        if (value == "") {
            return;
        }
        // construct body
        const date = new Date().valueOf();
        const p = this.props;
        const body = JSON.stringify({
            value,
            nickname: p.nickname,
            chat_with: p.chatWith,
            token: p.token
        });
        
        // generate new message token
        const token = generateToken(tokenLength);
        this.push({
            to_person: p.chatWith,
            contents: value,
            owner: p.nickname,
            date,
            token,
            notdelivered: true
        })

        // Send xhr if value is not ""
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => { 
            if (xhr.status == 200 && xhr.readyState == 4) {
                const resp = JSON.parse(xhr.response);
                if (resp.status == "success") {
                    this.props.markDelivered(token, resp.id);
                }
            }
        }

        // clear the textarea
        this.textarea.current.value = "";
        this.textarea.current.style.height = `${this.lineHeight}px`;

        xhr.open("POST", BACK_END + "/newmessage");
        xhr.send(body);

        this.scroll();
    }

    reply(e) {
        e.preventDefault();

        // actually edit the message
        const value = this.textarea.current.value.trim();
        if (value == "") {
            return;
        }

        // construct the body 
        const nickname = getCookie("nickname");
        const token = getCookie("token");
        const recipient = this.props.chatWith;

        // needed later in onreadystatechange handler
        const msg = this.props.msg;
        const date = new Date().valueOf();

        const body = JSON.stringify({
            value,
            id: this.props.msg.id,
            nickname,
            recipient,
            token
        });

        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                if (response.status == "notlogged") {
                    this.props.redirect();
                    return;
                } else {
                    //push the new reply
                    const id = response.id;
                    const reply = {
                        date,
                        id,
                        contents: value,
                        image_url: "", //change this later to support images
                        owner: nickname,
                        to_person: recipient,
                        replied_id: msg.id,
                        replied_date: msg.date,
                        replied_contents: msg.contents,
                        replied_deleted: msg.deleted,
                        replied_owner: msg.owner,
                        replied_to_person: msg.to_person
                    }

                    this.push(reply);
                }
            }
        }

        this.props.closeAfterReply(value);
        xhr.open("POST", BACK_END + "/replytomsg");
        xhr.send(body);

    }

    edit(e) {
        e.preventDefault();

        // actually edit the message
        const value = this.textarea.current.value.trim();
        if (value == "") {
            return;
        }

        // if new value is not equal to the previous one
        if (value != this.props.msg.contentes) {
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.status == 200 && xhr.readyState == 4) {
                    if (xhr.response == "notlogged") {
                        this.props.redirect();
                    }
                }
            }

            // construct the body 
            const nickname = getCookie("nickname");
            const token = getCookie("token");

            const body = JSON.stringify({
                value,
                id: this.props.msg.id,
                nickname,
                token
            });

            xhr.open("POST", BACK_END + "/editmsg");
            xhr.send(body);
        }
        this.props.closeAfterEditting(value);
    }

    render() {
        const props = this.props;
        const editting = props.editting;
        const replying = props.replying;
        const style = editting || replying ? { zIndex: 11 } : {};

        return <div ref={this.selfContainer} style={style} className={styles.MainContainer}>
            <div className={styles.Container}>
                {replying ? <Reply contents={props.msg.contents} author={props.msg.owner} /> : ""}
                <div className={styles.Textarea}>
                    <textarea onInput={editting || replying ? this.scroll : this.scrollWW} ref={this.textarea} maxLength="1500"></textarea>
                </div>
                <div className={styles.image}>
                    <img src="/assets/icons/messages/send.svg" alt="Send message" />
                    <div onMouseDown={editting ? this.edit : (replying ? this.reply : this.send)} className={Cover}></div>
                </div>
            </div>
        </div>
    }
}

New.propTypes = {
    pushMsg: PropTypes.func,
    container: PropTypes.object,
    msgcontainer: PropTypes.object,
    scroll: PropTypes.func,
    overflow: PropTypes.bool,
    closeAfterEditting: PropTypes.func,
    closeAfterReply: PropTypes.func,
    current: PropTypes.string,
    id: PropTypes.number,
    currentAuthor: PropTypes.string
}

export default New;