import React from "react";
import { Redirect, withRouter } from "react-router-dom";

import Menu from "./menu/Menu";
import Info from "./info/Info";
import Messages from "./messages/Messages";
import New from "./newmsg/New";
import styles from "./styles.scss";
import BigSpinner from "utils/spinner/Spinner-container";
import { BACK_END, MPR, DSD } from "presets";
import { getCookie } from "utils/cookies/getCookie";

const CHECKINTERVAL = 1000;

class Specific extends React.Component {
    constructor(props) {
        super(props);

        this.nickname = getCookie("nickname");
        this.token = getCookie("token");

        this.chatWith = props.match.params.with;
        this.container = React.createRef();
        this.specificContainer = React.createRef();
        this.textarea = React.createRef();

        this.state = {
            msg: {},
            bigSpinner: true,
            loading: true,
            redirect: false,
            messages: [],
            ready: false,
            offset: 0,
            max: false,
            smallSpinner: false,
            shouldScroll: true,
            menuOpen: false,
            editting: false,
            replying: false,
            tValue: ""
        }

        // suspend is used to disable scrolling events
        this.suspend = false;

        // bind methods 
        this.pushMessage = this.pushMessage.bind(this);
        this.scrollWindow = this.scrollWindow.bind(this);
        this.hideSpinner = this.hideSpinner.bind(this);
        this.windowScroll = this.windowScroll.bind(this);
        this.notInitial = this.notInitial.bind(this);
        this.openMenu = this.openMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
        this.reply = this.reply.bind(this);
        this.closeAfterDelete = this.closeAfterDelete.bind(this);
        this.closeAfterWipe = this.closeAfterWipe.bind(this);
        this.editMessage = this.editMessage.bind(this);
        this.closeAfterEditting = this.closeAfterEditting.bind(this);
        this.closeAfterReply = this.closeAfterReply.bind(this);
        this.markDelivered = this.markDelivered.bind(this);
        this.redirect = this.redirect.bind(this);
        this.checkForUpdate = this.checkForUpdate.bind(this);
        this.dealWithMessage = this.dealWithMessage.bind(this);
    }

    componentDidUpdate(_, prevState) {
        if (!prevState.max == this.state.max) {
            if (this.state.max) {
                window.removeEventListener("scroll", this.windowScroll);
            } else {
                window.addEventListener("scroll", this.windowScroll);
            }
        }
    }

    componentDidMount() {
        // get initial messages 
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const resp = JSON.parse(xhr.response);
                const status = resp.status;
                const msgs = resp.messages;

                switch (status) {
                    case "notlogged":
                        this.setState({ redirect: true });
                        break;
                    case "chatexists":
                        this.state.messages.unshift(...msgs);
                        const length = msgs.length;

                        // Build a new state
                        const newState = {};
                        newState.messages = this.state.messages;
                        newState.offset = this.state.offset + length;
                        if (length < MPR) {
                            newState.max = true;
                        }
                        if (msgs.length >= MPR) {
                            // set window event handler 
                            window.addEventListener("scroll", this.windowScroll);
                        } else {
                            newState.max = true;
                        }
                        this.setState(newState);
                    case "newchat":
                        this.setState({ loading: false });
                        break;
                }
            }
        }

        const body = JSON.stringify({
            nickname: this.nickname,
            token: this.token,
            chat_with: this.chatWith
        })

        xhr.open("POST", BACK_END + "/getinitialmsgs");
        xhr.send(body);

        // Declare the component ready 
        this.setState({ ready: true });

        // set the interval to check for updates
        this.checkUpdates = setTimeout(this.checkForUpdate, CHECKINTERVAL);
    }

    componentWillUnmount() {
        // remove the event handler 
        window.removeEventListener("scroll", this.windowScroll);

        // don't check for updates
        clearTimeout(this.checkUpdates);
    }

    dealWithMessage(changes) {
        let messages = this.state.messages;
        changes.forEach((msg) => {
            if (msg.type == 'new') {
                delete msg.type;
                messages.push(msg);
                return;
            }

            if (msg.type == 'updated') {
                delete msg.type;
                const index = messages.findIndex((value) => {
                    if (value.id == msg.id) return true;
                    return false
                });
                messages[index] = msg;
            }
        });

        this.setState({ messages });
    }

    checkForUpdate() {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                if (response.status == "success") {
                    // check again after some time 
                    this.checkUpdates = setTimeout(this.checkForUpdate, CHECKINTERVAL);

                    const changes = response.messages;
                    if (changes.length == 0) return;
                    this.dealWithMessage(changes);
                } else if (response.status == "notlogged") {
                    this.history.push("login");
                }
            }
        }

        xhr.open("POST", BACK_END + "/checkformessages");

        // construct the body
        const body = JSON.stringify({
            token: this.token,
            nickname: this.nickname,
            chat_with: this.chatWith
        });

        xhr.send(body);
    }

    windowScroll() {
        if (this.suspend || this.state.max) return;
        const scroll = window.pageYOffset;
        if (scroll <= DSD) {
            this.suspend = true;
            this.loadMore();
        }
    }

    loadMore() {
        // activate spinner 
        this.setState({ smallSpinner: true });

        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                if (response.status == "notlogged") {
                    this.setState({ redirect: true });
                } else if (response.status == "chatexists") {
                    const msgs = response.messages;
                    this.state.messages.unshift(...msgs);
                    const length = msgs.length;
                    // Build a new state
                    const newState = {};
                    newState.messages = this.state.messages;
                    newState.offset = this.state.offset + length;
                    if (length < MPR) {
                        newState.max = true;
                    }

                    // scroll window 
                    window.scrollBy(0, 1);

                    // hide spinner 
                    newState.smallSpinner = false;

                    // set shouldScroll to false
                    newState.shouldScroll = false;

                    this.setState(newState);
                }

                // Toggle the suspend variable
                this.suspend = false;
            }
        }

        // construct body
        const body = JSON.stringify({
            nickname: this.nickname,
            token: this.token,
            chat_with: this.chatWith,
            offset: this.state.offset
        });

        xhr.open("POST", BACK_END + "/getmsgs");
        xhr.send(body);
    }

    scrollWindow() {
        const container = document.querySelector("body");
        if (container) {
            const offset = container.scrollHeight;
            window.scrollTo(0, offset);
        }
    }

    pushMessage(msg) {
        const msgs = this.state.messages;
        msgs.push(msg);
        this.setState({ messages: msgs, shouldScroll: true });
    }

    hideSpinner() {
        this.setState({ bigSpinner: false });
    }

    notInitial() {
        this.setState({ initial: false });
    }

    // open menu 
    openMenu(message) {
        // body's hidden overflow
        const body = document.querySelector("body");
        body.style.height = "100vh";
        body.style.overflow = "hidden";

        // current textarea value 
        const tValue = this.textarea.current.value;

        this.setState({
            menuOpen: true, msg: message, tValue
        });
    }

    // close menu 
    closeMenu() {
        // body's normal overflow
        const body = document.querySelector("body");
        body.style.height = "auto";
        body.style.overflow = "visible";

        // restore the previous textarea value
        this.textarea.current.value = this.state.tValue;

        this.setState({ menuOpen: false, replying: false, editting: false, id: -1 });
    }

    // reply to messages
    reply() {
        this.setState({ replying: true });
        const textarea = this.textarea.current;
        textarea.value = "";
        textarea.focus();
    }

    closeAfterReply(id) {
        this.setState({ replying: false });
        this.closeMenu();
    }

    closeAfterDelete(id) {
        // change messages
        const messages = this.state.messages.map((msg) => {
            if (msg.id == id) {
                msg.deleted = true;
            }
            return msg;
        });
        this.setState({ messages });
        this.closeMenu();
    }

    closeAfterWipe(id) {
        // change messages
        const messages = this.state.messages.filter((msg) => msg.id != id);
        this.setState({ messages });
        this.closeMenu();
    }

    editMessage() {
        this.setState({ editting: true });
        const textarea = this.textarea.current;
        textarea.value = this.state.msg.contents;
        textarea.focus();
    }

    closeAfterEditting(value) {
        this.closeMenu();
        // Change messages
        const id = this.state.msg.id;
        const newMessages = this.state.messages.map((msg) => {
            if (msg.id == id) {
                msg.contents = value
            }

            return msg;
        });

        this.setState({ editting: false, messages: newMessages });
    }

    markDelivered(token, id) {
        const messages = this.state.messages.map((msg) => {
            if (msg.token && msg.token == token) {
                const obj = Object.assign({}, msg, { id, notdelivered: false });
                delete obj["notdelivered"];
                delete obj["token"];
                return obj;
            }
            return msg;
        })
        this.setState({ messages });
    }

    redirect() {
        this.setState({ redirect: true });
    }

    render() {
        const state = this.state;

        const showSpinner = state.loading || state.bigSpinner || !state.ready;

        // is menu open
        const menuOpen = state.menuOpen;

        // check if own
        let own = false;
        if (state.msg.owner == this.nickname) {
            own = true;
        }

        if (state.redirect) {
            return <Redirect to="/login" />
        }

        return <React.Fragment>
            {showSpinner ? <BigSpinner /> : ""}
            <div ref={this.specificContainer} className={styles.Container}>
                <Info container={this.specificContainer} hideSpinner={this.hideSpinner} nickname={this.chatWith} />
                <Messages shouldScroll={state.shouldScroll} spinner={state.smallSpinner} ownnickname={this.nickname} scroll={this.scrollWindow} menu={this.openMenu} container={this.container} messages={state.messages} />
                {menuOpen ? <Menu id={state.msg.id} edit={this.editMessage} editting={state.editting} closeAfterEditting={this.closeAfterEditting} closeAfterDelete={this.closeAfterDelete} closeAfterWipe={this.closeAfterWipe} close={this.closeMenu} reply={this.reply} replying={state.replying} deleted={state.msg.deleted} own={own} /> : ""}
                <New ready={state.ready} current={state.currentValue} msg={state.msg} textarea={this.textarea} closeAfterEditting={this.closeAfterEditting} redirect={this.redirect} closeAfterReply={this.closeAfterReply} editting={state.editting} container={this.specificContainer} msgcontainer={this.container} chatWith={this.chatWith} pushMsg={this.pushMessage} scroll={this.scrollWindow} token={this.token} nickname={this.nickname} replying={state.replying} markDelivered={this.markDelivered} />
            </div>
        </React.Fragment>
    }
}
Specific = withRouter(Specific);
export default Specific;