import React from "react";
import PT from "prop-types";

import { Redirect } from "react-router-dom";
import { BACK_END } from "presets";
import { getCookie } from "utils/cookies/getCookie";
import styles from "./styles.scss";

class Menu extends React.Component {
    constructor(props) {
        super(props);

        // initial state
        this.state = {
            redirect: false
        }

        // bind functions 
        this.delete = this.delete.bind(this);
        this.wipe = this.wipe.bind(this);
        this.clickOnMain = this.clickOnMain.bind(this);
        this.clickOnReply = this.clickOnReply.bind(this);
        this.clickOnDelete = this.clickOnDelete.bind(this);
        this.clickOnCancel = this.clickOnCancel.bind(this);
        this.clickOnWipe = this.clickOnWipe.bind(this);
        this.clickOnEdit = this.clickOnEdit.bind(this);
    }

    wipe() {
        const id = this.props.id;
        const nickname = getCookie("nickname");
        const token = getCookie("token");
        if (token == "" || nickname == "") {
            // definetely not logged in
            this.setState({ redirect: true });
        } else {
            // wipe the message 
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.status == 200 && xhr.readyState == 4) {
                    if (xhr.response == "notlogged") {
                        this.setState({ redirect: true });
                    }
                }
            }

            xhr.open("POST", BACK_END + "/wipemsg");
            const body = JSON.stringify({ nickname, id, token });
            xhr.send(body);

            // Alter messages
            this.props.closeAfterWipe(id);
        }
    }

    delete() {
        const id = this.props.id;
        const nickname = getCookie("nickname");
        const token = getCookie("token");
        if (token == "" || nickname == "") {
            // definetely not logged in
            this.setState({ redirect: true });
        } else {
            // XHR for deleting messages
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.status == 200 && xhr.readyState == 4) {
                    if (xhr.response == "notlogged") {
                        this.setState({ redirect: true });
                    }
                }
            }

            xhr.open("POST", BACK_END + "/deletemsg");
            const body = JSON.stringify({ nickname, id, token });
            xhr.send(body);

            // Alter messages
            this.props.closeAfterDelete(id);
        }
    }

    clickOnReply(e) {
        e.stopPropagation();
        this.props.reply();
    }

    clickOnMain(e) {
        e.stopPropagation();
        this.props.close();
    }

    clickOnDelete(e) {
        e.stopPropagation();
        this.delete();
    }

    clickOnCancel(e) {
        e.stopPropagation();
        this.props.close();
    }

    clickOnWipe(e) {
        e.stopPropagation();
        this.wipe();
    }

    clickOnEdit(e) {
        e.stopPropagation();
        this.props.edit();
    }

    render() {
        const props = this.props;
        const deleted = props.deleted;
        const editting = props.editting;
        const replying = props.replying;
        const own = props.own;

        if (this.state.redirect) {
            return <Redirect to="/login" />
        }

        if (deleted) {
            return <div onClick={this.clickOnMain} className={styles.MainContainer}>
                <div className={styles.Container}>
                    <button onClick={this.clickOnWipe} className={styles.control}>Delete for me</button>
                    <button onClick={this.clickOnCancel} className={styles.control}>Cancel</button>
                </div>
            </div>
        }

        return <div onClick={this.clickOnMain} className={styles.MainContainer}>
            <div className={styles.Container}>
                {
                    editting ?
                        <p className={styles.editting}>Edit now</p> :
                        replying ? <p className={styles.editting}>Reply now</p> :
                            <React.Fragment>
                                <button onClick={this.clickOnReply} className={styles.control}>Reply</button>
                                <button onClick={this.clickOnWipe} className={styles.control}>Delete for me</button>
                                <button onClick={this.clickOnCancel} className={styles.control}>Cancel</button>
                                {!deleted && own ?
                                    <button onClick={this.clickOnDelete} className={styles.control}>Delete for everyone</button> : ""
                                }
                                {own ? <button onClick={this.clickOnEdit} className={styles.control}>Edit</button> : ""}
                            </React.Fragment>
                }
            </div>
        </div>
    }
}

Menu.propTypes = {
    deleted: PT.bool,
    close: PT.func,
    closeAfterDelete: PT.func,
    closeAfterWipe: PT.func,
    editting: PT.bool,
    edit: PT.func,
    closeAfterEditting: PT.func,
    own: PT.bool
}

export default Menu;