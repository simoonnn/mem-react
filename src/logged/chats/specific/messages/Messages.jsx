import React from "react";
import PropTypes from "prop-types";

import { Cover } from "classes.scss";
import Spinner from "utils/spinner/Spinner";
import styles from "./styles.scss";
import Single from "./single/Single";

class Messages extends React.Component {
    constructor(props) {
        super(props);

        // extract from props
        this.ownNickname = this.props.ownnickname;
    }

    componentDidUpdate() {
        if (this.props.shouldScroll && this.props.spinner == false) {
            this.props.scroll();
        }
    }

    render() {
        const showSpinner = this.props.spinner;
        const container = this.props.container;

        // Some date related stuff 
        const DAY = 1000 * 3600 * 24;
        this.cd = 0;
        let date;

        const messages = this.props.messages.map((obj) => {
            let displayDate = false;
            const own = this.ownNickname == obj.owner;
            // choose whether to display date
            date = obj.date;
            let days = Math.floor(date / DAY)
            if (days - this.cd >= 1) {
                displayDate = true;
                this.cd = days;
            }
            return <Single menu={this.props.menu} displayDate={displayDate} message={obj} key={obj.token || obj.id} own={own} />
        });

        // set a different z-index to covering container
        const style = {
            zIndex: 5
        }

        return <div ref={container} className={styles.Container}>
            {showSpinner ? <Spinner cls={styles.Spinner} /> : ""}
            {messages}
            <div style={style} className={Cover}></div>
        </div>
    }
}

Messages.propTypes = {
    menu: PropTypes.func,
    ownnickname: PropTypes.string,
    messages: PropTypes.arrayOf(PropTypes.object),
    container: PropTypes.object,
    scroll: PropTypes.func,
    smallSpinner: PropTypes.bool,
    shouldScroll: PropTypes.bool
}

export default Messages;