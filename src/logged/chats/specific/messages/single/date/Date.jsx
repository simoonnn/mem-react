import React from "react";

import styles from "./styles.scss";
import PT from "prop-types";
import {CALENDAR} from "presets";

class DisplayDate extends React.Component {
    render() {
        // construct date
        let date;
        const dateObject = new Date(this.props.epoch);
        const now = new Date().valueOf();
        const diff = now - this.props.epoch;

        // one year in milliseconds
        const ONEYEAR = 1000 * 3600 * 24 * 365;
        if (diff >= ONEYEAR) {
            // format with year
            date = dateObject.toLocaleDateString(navigator.language, {
                year: "numeric",
                day: "numeric",
                month: "long"
            })
        } else {
            // format without year
            date = dateObject.toLocaleDateString(navigator.language, {
                day: "numeric",
                month: "long"
            })
        }

        return <div className={styles.Container}>
            <p>{date}</p>
        </div>
    }
}

DisplayDate.propTypes = {
    epoch: PT.number
}

export default DisplayDate;
