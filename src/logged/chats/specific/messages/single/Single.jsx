import React from "react";
import PropTypes from "prop-types";

import Replied from "./replied/Replied";
import { Cover } from "classes.scss";
import styles from "./styles.scss";
import DisplayDate from "./date/Date";

class Single extends React.Component {
    constructor(props) {
        super(props);
        
        // bind functions 
        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        e.stopPropagation();
        e.preventDefault();

        if (this.props.message.notdelivered) return;

        const message = this.props.message;
        this.props.menu(message);
    }

    render() {
        const own = this.props.own;
        const msg = this.props.message;
        const className = own ? styles.Own : styles.Others;

        // should render Reply
        const shouldReply = msg.replied_id > 0;

        // Construct date
        let date = new Date(msg.date);
        let localTime = date.toLocaleTimeString(navigator.language, {
            hour: "2-digit",
            minute: "2-digit"
        });

        // Decide the contents className
        const contents = msg.deleted ? <p className={styles.deleted}>
            This messages was deleted
        </p> :
            <p className={styles.contents}>
                {msg.contents}
            </p>

        // decide what image to display
        let src;
        if (msg.notdelivered) {
            src = "/assets/icons/miscellaneous/loading.svg";
        } else {
            src = "/assets/icons/miscellaneous/loaded.svg";
        }

        return <React.Fragment>
            {this.props.displayDate ? <DisplayDate epoch={msg.date} /> : ""}
            <div unselectable="on" className={className}>
                {shouldReply ? <Replied message={msg} /> : ""}
                {contents}
                <p className={styles.date}>{localTime}</p>
                <img className={styles.image} src={src} />
                <div onClick={this.onClick} className={Cover}></div>
            </div>
        </React.Fragment>
    }
}

Single.propTypes = {
    message: PropTypes.object,
    own: PropTypes.bool,
    menu: PropTypes.func
}

export default Single;