import React from "react";
import PT from "prop-types";
import classnames from "classnames";

import { getCookie } from "utils/cookies/getCookie";
import { replyLength } from "presets";
import styles from "./styles.scss";


class Replied extends React.Component {
    render() {
        const message = this.props.message;
        const del = message.replied_deleted;

        // cut off the excessive contents
        let contents = message.replied_contents;
        if (contents.length > replyLength) {
            contents = contents.substring(0, replyLength);
            contents += "...";
        }

        // decide who the author is
        const repliedOwner = message.replied_owner;
        const nickname = getCookie("nickname");
        let author = repliedOwner;

        if (repliedOwner == nickname) {
            author = "You";
        }

        // determine the classname 
        let clsName = styles.Container;
        const owner = message.owner;

        if (owner != nickname) {
            clsName = classnames(clsName, styles.GreenContainer);
        }

        return <div className={clsName}>
            <p className={styles.author}>{author}</p>
            <p className={styles.text}>{contents}</p>
        </div>
    }
}

Replied.propTypes = {
    message: PT.object
}

export default Replied;