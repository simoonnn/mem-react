import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import { Cover } from "classes.scss";
import styles from "./styles.scss";
import Avatar from "./avatar/Avatar";

class Info extends React.Component {
    constructor(props) {
        super(props);

        // create refs
        this.fake = React.createRef();
        this.container = React.createRef();
    }

    componentDidMount() {
        setTimeout(() => {
            const specificContainer = this.props.container.current;
            const container = this.container.current;
            if (specificContainer && container) {
                // get container's height 
                const height = container.scrollHeight;
                specificContainer.style.paddingTop = `${height}px`;

                // hide Spinner
                this.props.hideSpinner();
            }
        }, 300);
    }

    render() {
        const nickname = this.props.nickname;

        return <div className={styles.Fake}>
            <div ref={this.container} className={styles.Container}>
                <div className={styles.Grid} >
                    <Link to="/chats/"><img className={styles.button} src="/assets/icons/back.svg" /></Link>
                    <Avatar nickname={nickname} />
                    <div className={styles.nickname}>
                        <p>{nickname}</p>
                        {/* <div className={Cover}></div> */}
                    </div>
                </div>
                <div className={styles.Space}></div>
            </div>
        </div>
    }
}

Info.propTypes = {
    nickname: PropTypes.string,
    container: PropTypes.object,
    hideSpinner: PropTypes.func
}

export default Info;