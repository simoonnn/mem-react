import React from "react";
import PropTypes from "prop-types";

import { Cover } from "classes.scss";
import styles from "./styles.scss";
import { BACK_END } from "presets";
import Spinner from "utils/spinner/Spinner";
import { DUA } from "presets";

class Avatar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fetched: false,
            avatarUrl: "",
            loaded: false
        };

        // extract from props 
        this.requested = props.nickname;

        // bind functions 
        this.onload = this.onload.bind(this);
    }
    componentDidMount() {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                const response = JSON.parse(xhr.response);
                this.setState({
                    fetched: true,
                    avatarUrl: response.avatar_url
                });
            }
        }

        const body = JSON.stringify({
            nickname: this.requested
        });

        xhr.open("POST", BACK_END + "/getavatar");
        xhr.send(body);
    }

    onload() {
        this.setState({ loaded: true });
    }

    render() {
        const state = this.state;

        // construct image 
        let image;
        let url;
        if (!state.avatarUrl) {
            url = DUA
        } else {
            url = state.avatarUrl;
        }

        if (state.fetched) {
            image = <img onLoad={this.onload} src={url} />
        } else {
            image = ""
        }

        // Spinner 
        const spinner = <Spinner cls={styles.Spinner} />
        const loaded = state.loaded;

        return <div className={styles.Container}>
            {!loaded ? spinner : ""}
            <div className={!loaded ? styles.Hidden : styles.Wrapper}>
                <div className={styles.Avatar}>
                    {image}
                </div>
            </div>
            <div className={Cover}></div>
        </div>
    }
}

Avatar.propTypes = {
    nickname: PropTypes.string
}

export default Avatar;