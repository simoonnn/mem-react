import React from "react";
import PT from "prop-types";
import { Redirect } from "react-router-dom";

import styles from "./styles.scss";
import Spinner from "utils/spinner/Spinner";
import { BACK_END, DUA, DAY } from "presets";

class SingleChat extends React.Component {
    constructor(props) {
        super(props);

        // Initial state 
        this.state = {
            loading: true,
            avatarURL: "N",
            redirect: false
        }

        // bind functions 
        this.onload = this.onload.bind(this);
        this.redirect = this.redirect.bind(this);
    }

    componentDidMount() {
        // fetch avatar_url
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                this.setState({ avatarURL: response.avatar_url });
            }
        }

        // construct body
        const nickname = this.props.chat.nickname;
        const body = JSON.stringify({
            nickname
        });

        xhr.open("POST", BACK_END + "/getavatar");
        xhr.send(body);

        // decide what the date should be like 
        const ts = this.props.chat.date;
        const dt = new Date(+ts);
        const days = Math.round(ts / DAY);

        // choose the best case scenario
        switch (true) {
            case (days < 365):
                if (days == 1) {
                    this.date = `1 day ago`;
                } else {
                    this.date = `${days} days ago`;
                }
            case (days > 365):
                const years = Math.round(days / 365);
                if (years == 1) {
                    this.date = `1 year ago`;
                } else {
                    this.date = `${years} days ago`;
                }
            default:
                this.date = dt.toLocaleTimeString(navigator.string, {
                    hour: "2-digit",
                    minute: "2-digit"
                });
        }
    }

    // When the avatar is loaded...
    onload() {
        // don't show the spinner 
        this.setState({ loading: false });
    }

    // redirect to specific chat
    redirect() {
        this.setState({ redirect: true });
    }

    render() {
        const c = this.props.chat;
        const loading = this.state.loading;
        const deleted = c.deleted;

        // should redirect 
        const redirect = this.state.redirect;
        if (redirect) {
            return <Redirect to={`/chats/${c.nickname}`} />
        }

        // avatarURL loaded 
        const loaded = this.state.avatarURL != "N";
        const url = this.state.avatarURL || DUA;

        return <div className={styles.Container}>
            <div onClick={this.redirect} className={styles.Cover}></div>
            {loading ? <Spinner cls={styles.Spinner} /> : ""}
            <div className={loading ? styles.Hidden : styles.Border} >
                <div className={styles.Avatar}>
                    {loaded ? <img onLoad={this.onload} src={url} alt="Avatar" /> : ""}
                </div>
            </div>
            <div className={styles.Double}>
                <p className={styles.nickname}>{c.nickname}</p>
                {
                    deleted ?
                        <p className={styles.deleted}>The message was deleted</p> :
                        <p className={styles.text}>{c.contents}</p>
                }
            </div>
            <p className={styles.time}>{this.date}</p>
        </div>
    }
}

SingleChat.propTypes = {
    chat: PT.shape({
        nickname: PT.string.isRequired,
        contents: PT.string.isRequired,
        date: PT.any.isRequired
    })
}

export default SingleChat;