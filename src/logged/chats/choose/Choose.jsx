import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { BACK_END } from "presets";
import Results from "./results/Results";
import { changeNavVis } from "src/redux/actions";
import styles from "utils/search/styles.scss";
import { getCookie } from "utils/cookies/getCookie";

class Search extends React.Component {
    constructor(props) {
        super(props);

        // Create a reference to input 
        this.input = React.createRef();

        this.state = {
            open: false,
            users: [],
            initial: true,
            spinner: true,
            redirect: false,
            bSpinner: false,
            redirectToChat: false
        };

        this.interval = 600;
        this.timeout = null;
        this.offset = 0;
        this.nickname = getCookie("nickname");
        this.token = getCookie("token");

        //xhr's
        this.searchxhr = new XMLHttpRequest();
        this.getxhr = new XMLHttpRequest();

        // extract from props
        this.close = this.props.hide;
        this.type = this.props.type;

        // bind functions
        this.handler = this.delaySend();
        this.loadChats = this.loadChats;
        this.redirectToChat = this.redirectToChat.bind(this);
    }

    componentDidUpdate() {
        if (!this.props.show) {
            clearTimeout(this.timeout);
            this.props.changeVisibility(true);
        } else {
            this.props.changeVisibility(false);
        }
    }

    componentDidMount() {
        this.props.changeVisibility(false);
        this.loadChats();
    }

    componentWillUnmount() {
        this.searchxhr.abort();
        this.getxhr.abort();

        // make the body overflow visible
        const body = document.querySelector("body");
        body.style.overflow = "visible";
    }

    loadChats() {
        this.setState({ spinner: true });

        this.getxhr = new XMLHttpRequest();
        this.getxhr.onreadystatechange = () => {
            if (this.getxhr.status == 200 && this.getxhr.readyState == 4) {
                const notLogged = this.getxhr.response == "notlogged";
                if (notLogged) {
                    this.setState({ redirect: true });
                    return;
                }
                const resp = JSON.parse(this.getxhr.response);
                this.setState({ users: resp, spinner: false, initial: false });
            }
        }

        const body = JSON.stringify({ nickname: this.nickname, token: this.token });

        this.getxhr.open("POST", BACK_END + "/openchat");
        this.getxhr.send(body);
    }

    sendXHR(value) {
        this.setState({ spinner: true });
        this.searchxhr = new XMLHttpRequest();
        this.searchxhr.onreadystatechange = () => {
            if (this.searchxhr.status == 200 && this.searchxhr.readyState == 4) {
                const notLogged = this.searchxhr.response == "notlogged";
                if (notLogged) {
                    this.setState({ redirect: true });
                    return;
                }

                const resp = JSON.parse(this.searchxhr.response);
                this.setState({ users: resp, spinner: false });
            }
        }

        const body = JSON.stringify({ nickname: this.nickname, token: this.token, value });

        this.searchxhr.open("POST", BACK_END + "/searchchats");
        this.searchxhr.send(body);
    }

    redirectToChat(user) {
        return () => {
            this.redirectUser = user;
            this.setState({ redirectToChat: true });
        }
    }

    delaySend() {
        let prValue = "";
        const handler = () => {
            clearTimeout(this.timeout);
            const curValue = this.input.current.value;

            if (prValue != "" && curValue == "") {
                this.loadChats();
                prValue = "";
                return;
            }

            if (prValue == curValue || curValue == "") {
                return;
            }

            this.searchxhr.abort();
            this.timeout = setTimeout(() => {
                this.sendXHR(curValue);
                prValue = curValue;
            }, this.interval);
        }
        return handler.bind(this);
    }

    render() {
        const state = this.state;
        const show = this.props.show;
        const hStyles = {
            display: "none"
        };

        // If not logged in 
        if (state.redirect) {
            return <Redirect to="/login" />
        }

        // Redirecting to a specific chat
        if (state.redirectToChat) {
            return <Redirect to={`/chats/${this.redirectUser}`} />
        }


        return <div style={!show ? hStyles : {}} className={styles.FullContainer}>
            <div className={styles.Top}>
                <img onClick={this.close} className={styles.Back} src="/assets/icons/back.svg" />
                <div className={styles.Input}>
                    <input onInput={this.handler} ref={this.input} placeholder="Search people" type="text" maxLength="60" />
                    <button><img src="/assets/icons/search.svg" /></button>
                </div>
            </div>
            <Results initial={this.state.initial} redirect={this.redirectToChat} bigSpinner={state.bSpinner} title={this.title} spinner={state.spinner} users={state.users} />
        </div>

    }
}

Search.propTypes = {
    title: PropTypes.string,
    hide: PropTypes.func,
    type: PropTypes.string,
    show: PropTypes.bool
}

//Redux stuff
function mapDispatchToProps(dispatch) {
    return {
        changeVisibility: (show) => dispatch(changeNavVis(show))
    }
}
Search = connect(null, mapDispatchToProps)(Search);

export default Search;