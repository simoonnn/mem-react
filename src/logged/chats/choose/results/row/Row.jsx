import React from "react";
import PropTypes from "prop-types";

import { DUA } from "presets";
import styles from "utils/search/results/row/styles.scss";
import Spinner from "utils/spinner/Spinner";

class Row extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            redirect: false
        }

        // bind functions 
        this.onload = this.onload.bind(this);
    }


    onload() {
        this.setState({ loaded: true });
    }

    render() {
        const p = this.props;
        const loaded = this.state.loaded;

        return <div onClick={p.redirect(p.nickname)} className={styles.Container}>
            <div className={styles.Cover}></div>
            <div className={loaded ? styles.Border : styles.NoDisplay}>
                <div className={styles.Wrapper}>
                    {!loaded ? <Spinner cls={styles.Spinner} /> : ""}
                    <img onLoad={this.onload} src={p.avatar_url || DUA} />
                </div>
            </div>
            <p>{p.nickname}</p>
        </div>
    }
}

Row.propTypes = {
    avatar_url: PropTypes.string.isRequired,
    nickname: PropTypes.string.isRequired,
    redirect: PropTypes.func
}

Row.defaultProps = {
    redirect: () => console.log("Default redirect to chat")
}

export default Row;