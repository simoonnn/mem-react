import React from "react";
import PropTypes from "prop-types";

import Row from "./row/Row";
import styles from "utils/search/results/styles.scss";

import { Header1 } from "utils/headers/Headers";
import Spinner from "utils/spinner/Spinner";
import BigSpinner from "utils/spinner/Spinner-container";

class Results extends React.Component {
    render() {
        const props = this.props;
        const spinner = props.spinner;

        const users = props.users || [];
        const rows = users.map((user) => {
            return <Row key={user.nickname} redirect={this.props.redirect} nickname={user.nickname} avatar_url={user.avatar_url} />
        });

        const no_rows = rows.length == 0;
        const initial = props.initial;

        return <React.Fragment>
            {props.bigSpinner ? <BigSpinner /> : ""}
            <div className={styles.Container}>
                {initial ? "" : <Header1 label={no_rows ? "No rows" : "Text to:"} />}

                {spinner ? <div className={styles.SpinnerContainer}><Spinner cls={styles.Spinner} /></div> :
                    <div className={styles.Rows}>{rows}</div>}

            </div>
        </React.Fragment>
    }
}

Results.propTypes = {
    spinner: PropTypes.bool,
    users: PropTypes.arrayOf(PropTypes.object),
    redirect: PropTypes.func,
    bigSpinner: PropTypes.bool
}

export default Results;