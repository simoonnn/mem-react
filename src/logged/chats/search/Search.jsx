import React from "react";
import PropTypes from "prop-types";

import styles from "./styles.scss";
import { BACK_END } from "presets";
import { getCookie } from "utils/cookies/getCookie";

const timer = 500;

class Search extends React.Component {
    constructor(props) {
        super(props);

        // create refs 
        this.container = React.createRef();
        this.input = React.createRef();

        // timeout 
        this.timeout = null;

        // bind functions 
        this.generateHandler = this.generateHandler.bind(this);
    }

    componentDidMount() {
        setTimeout(() => {
            if (this.container.current) {
                const height = this.container.current.scrollHeight;
                this.props.setOffset(height);
            }
        }, 200);
    }

    generateHandler() {
        let prevValue;
        return () => {
            clearTimeout(this.timeout);
            this.props.toggleSpinner(true);
            // Input value must have some meaning 
            const value = this.input.current.value;
            if (value == "") {
                this.props.returnInitial();
                this.props.toggleSpinner(false);

                return;
            }
            this.timeout = setTimeout(() => {
                // If value equals to prevValue
                if (value == prevValue) return;

                const xhr = new XMLHttpRequest();
                xhr.onreadystatechange = () => {
                    if (xhr.status == 200 && xhr.readyState == 4) {
                        const response = JSON.parse(xhr.response);
                        this.props.setChats(response);
                    }
                }

                xhr.open("POST", BACK_END + "/searchexistingchats");

                // construct body 
                const nickname = getCookie("nickname");
                const token = getCookie("token");
                const body = JSON.stringify({
                    nickname,
                    token,
                    value
                });

                xhr.send(body);

                // Change prevValue
                prevValue = value;
            }, timer);
        }
    }

    render() {
        return <div ref={this.container} className={styles.Container}>
            <div className={styles.Input} >
                <input onFocus={this.open} onInput={this.generateHandler()} ref={this.input} placeholder="Search for someone" type="text" maxLength="60" />
                <img alt="Search" src="/assets/icons/search.svg" />
            </div>
        </div>
    }
}

Search.propTypes = {
    setOffset: PropTypes.func,
    setChats: PropTypes.func,
    returnInitial: PropTypes.func,
    toggleSpinner: PropTypes.func
}

export default Search