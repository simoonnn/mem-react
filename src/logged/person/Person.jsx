import React from "react";
 
import styles from "./styles.scss";
import Display from "../display/Display";
import Navbar from "utils/navbar/Navbar";
import {getCookie} from "utils/cookies/getCookie";
import {Redirect} from "react-router-dom";

class Person extends React.Component {
    constructor(props) {
        super(props);
        this.nickname = props.match.params.nickname;
    }
    render() {        
        const ownnickname = getCookie("nickname");
        if (ownnickname == this.nickname) {
            return <Redirect to="/" />
        }

        return <div className="logged-home-container">
            <Display own={false} nickname={this.nickname}/>
            <Navbar />
        </div>
    }
}

export default Person;