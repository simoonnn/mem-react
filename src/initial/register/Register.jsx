import React from "react";

import Form from "./form/Form";
import Navbar from "../navbar/Navbar";
import styles from "./styles.scss";

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.objectList = [
            {to: "/home", name: "Home"},
            {to: "/register", name: "Register", active: true},
            {to: "/login", name: "Login"}
        ];
    }
    render() {
        return <div className={styles.Container}>
            <Navbar objectList={this.objectList} />
            <Form />
        </div>
    }
}

export default Register;