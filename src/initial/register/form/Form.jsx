import React from "react";
import {Redirect} from "react-router-dom";


import styles from "./styles.scss";
// Handlers
import formIsValid from "./validators/formValid";
import checkPassword from "./validators/password";
import checkPasswordConfirmation from "./validators/passwordConfirmation";
import checkNick from "./validators/nickname";
import checkEmail from "./validators/email";
import closePopup from "./events/closePopup";

//Events
import buttonClick from "./events/buttonClick";
import delayCheck from "./events/delay";
import handleCheck from "./events/passCheck";

//Components and styles
import Spinner from "utils/spinner/Spinner-container";
import Popup from "./popup/Popup";
import Button from "./button/Button";
import {Email, Password, Nickname} from "./fields/Fields";
import {Header1} from "./headers/Headers";



/*
    The required form components will be: Password, Required, Optional
    They all will have required property: label
*/

class Form extends React.Component {
    constructor(props) {
        super(props);

        // References 
        this.email = React.createRef(); 
        this.password = React.createRef(); 
        this.passwordC = React.createRef();
        this.nickname = React.createRef();

        //State
        this.state = {
            eInvalid: false,
            eValid: false,
            eErrors: [],
            pInvalid: false,
            pValid: false,
            pHidden: true,
            pErrors: [],
            pcInvalid: false,
            pcHidden: true,
            pcValid: false,
            pcErrors: [],
            nValid: false,
            nInvalid: false,
            nErrors: [],
            formValid: false,
            popup: false,
            redirect: false,
            spinner: false
        };
        
        //Bind functions

        this.formIsValid = formIsValid.bind(this);
        this.delayCheck = delayCheck.bind(this);
        this.checkPassword = checkPassword.bind(this);
        this.checkNick = checkNick.bind(this);
        this.checkPasswordConfirmation = checkPasswordConfirmation.bind(this);
        this.checkEmail = checkEmail.bind(this)
        this.closePopup = closePopup.bind(this);
        this.handleCheck = handleCheck.bind(this);

        // Handlers
        this.emailHandler = {   
            onInput: delayCheck(this.checkEmail, 400, this.email)
        };
        this.passwordHandler = {
            onInput: delayCheck(this.checkPassword, 400, this.password)
        }
        this.passwordConfirmationHandler = {
            onInput: delayCheck(this.checkPasswordConfirmation, 400, this.passwordC)
        }
        this.nicknameHandler = {
            onInput: delayCheck(this.checkNick, 700, this.nickname)
        }

        // Submit form 
        this.buttonClick = buttonClick.bind(this);
    }

    render() {
        let state = this.state;
        // Redirect to home page
        if (state.redirect) {
            return <Redirect to="/login"/>
        }

        /* 
        Example: 
            <Email ref={this.email} handler={this.emailHandler} invalid={state.eInvalid} handler={this.handleLength} valid={state.eValid} errorList={state.eErrors} />
        */
       return <React.Fragment>
           {this.state.spinner ? <Spinner /> : ""}
           <div className={styles.Container}>
                <Header1 label="Register"/>
                <div className={styles.Form}>
                    <Nickname ref={this.nickname} handler={this.nicknameHandler} invalid={state.nInvalid} valid={state.nValid} errorList={state.nErrors} />

                    <Email ref={this.email} handler={this.emailHandler} invalid={state.eInvalid} valid={state.eValid} errorList={state.eErrors} />

                    <Password ref={this.password} label="Password" hidden={this.state.pHidden} handler={this.passwordHandler} handleCheck={this.handleCheck("pHidden")} invalid={state.pInvalid} valid={state.pValid} errorList={state.pErrors} />

                    <Password ref={this.passwordC} hidden={false} label="Confirm Password" hidden={this.state.pcHidden} handleCheck={this.handleCheck("pcHidden")} invalid={state.pcInvalid} handler={this.passwordConfirmationHandler} valid={state.pcValid} errorList={state.pcErrors} />
                </div>
                <Button valid={this.state.formValid} handler={this.buttonClick}  label="Submit"/>
    {this.state.popup ? <Popup onClose={this.closePopup} text="Check your email now" /> : ""}
            </div>
           </React.Fragment>
    }
}
export default Form;