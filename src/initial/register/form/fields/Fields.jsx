import PropTypes from "prop-types";
import React from "react";

import Checkbox from "./checkbox/Checkbox";
import ErrorContainer from "./error/Error-container";
import styles from "./styles.scss";

const Field = React.forwardRef((props, ref) => {
    let className = props.invalid ? styles.ContainerInvalid : styles.Container;
    className = props.valid ? styles.ContainerValid : className;

    return <div className={styles.Overall} >
        <div className={className}>
            <label className={styles.label}>{props.label}</label>
            <input className={styles.input} autoComplete={props.autocomplete} maxLength="60" {...props.handler} ref={ref} type={props.type}/>
        </div>
        {props.checkButton ?  <Checkbox label="Show Password" handleCheck={props.handleCheck} /> : ""}
        {props.invalid == true ? <ErrorContainer errorList={props.errorList}/> : ""}
    </div>
});

// Nickname 
export const Nickname = React.forwardRef((props, ref) => {
    return <Field label="Nickname" ref={ref} {...props} type="text" />
});

// Email field for our form
export const Email = React.forwardRef((props, ref) => {
    return <Field label="Email" autoComplete="email" ref={ref} {...props} type="text" />
});

// Password field (hidden)
export const Password = React.forwardRef((props, ref) => {
    return <Field autocomplete="password" ref={ref} checkButton={true} {...props} type={props.hidden ? "password" : "text"}/> 
})

// Types of props


Email.propTypes = Nickname.propTypes = Field.propTypes =  {
    errorList: PropTypes.array.isRequired,
    handler: PropTypes.objectOf(PropTypes.func),
    invalid: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired
};


Password.propTypes = {
    errorList: PropTypes.array.isRequired,
    handler: PropTypes.objectOf(PropTypes.func),
    label: PropTypes.string,
    valid: PropTypes.bool.isRequired,
    hidden: PropTypes.bool.isRequired,
    handleCheck: PropTypes.func.isRequired
};
