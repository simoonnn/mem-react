import React from "react";
import PropTypes from "prop-types";

import Error from "./Error";
import styles from "./styles.scss";

class ErrorContainer extends React.Component {
    render() {
        const errors = this.props.errorList.map((err) => {
            return <Error key={err} content={err} />
        });
        if (errors.length == 0) {
            return <React.Fragment/>
        }
        return <div className={styles.Container}>
            {errors}
        </div>
    }
}

ErrorContainer.propTypes = {
    errorList: PropTypes.array.isRequired
}

export default ErrorContainer;


