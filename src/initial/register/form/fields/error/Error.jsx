import React from "react";
import PropTypes from "prop-types";

import styles from "./styles.scss";

class Error extends React.Component {
    render() {
        return <p className={styles.error}><span>{this.props.content}</span></p>
    }
}

Error.propTypes = {
    content: PropTypes.string.isRequired
}

export default Error;