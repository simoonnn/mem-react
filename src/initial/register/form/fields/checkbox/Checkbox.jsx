import React from "react";
import PropTypes from "prop-types";

import styles from "./styles.scss";

class Checkbox extends React.Component {
    render() {
        return <div className={styles.MainContainer}>
            <span className={styles.label}>{this.props.label}</span>
                <label className={styles.Checkbox}>
                    <input onInput={this.props.handleCheck} className={styles.input} type="checkbox"/>
                    <p className={styles.main}></p>
                </label>
            </div>
    }
}

Checkbox.propTypes = {
    label: PropTypes.string.isRequired,
    handleCheck: PropTypes.func.isRequired
}



export default Checkbox;