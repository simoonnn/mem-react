import React from "react";
import PropTypes from "prop-types";

import styles from "./styles.scss";

export class Header1 extends React.Component {
    render() {
        return <h1 className={styles.Header}>{this.props.label}</h1>
    }
}


Header1.propTypes = {
    label: PropTypes.string.isRequired
}
