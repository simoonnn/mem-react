function checkEmail() {
    // Don't only check for validity of the email
    // Also check for availability 

    let newState = Object.assign({}, this.state);
    const contents = this.email.current.value.trim();
    const regex = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/

    // If email is not valid
    if (contents.match(regex) == null) {
        const errs = ["Please, fill in your real email"];
        this.setState({eInvalid: true, eValid: false, eErrors: errs});
        newState.eInvalid = true;
        newState.eValid = false;
    } else {
        this.setState({eInvalid: false, eValid: true, eErrors: []});
        newState.eInvalid = false;
        newState.eValid = true;
    }
    this.formIsValid(newState);
}

export default checkEmail;