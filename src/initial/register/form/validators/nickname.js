import {BACK_END} from "presets";

function checkNick() {
    let newState = Object.assign({}, this.state);
    const contents = this.nickname.current.value.trim();
    
    // is nickname valid? 

    const regex = /^[a-zA-Z0-9_$@]+$/

    // If nickname is not valid
    if (contents.match(regex) == null) {
        const errs = ["Only letters, numbers, _, @, and $"];
        this.setState({nInvalid: true, nValid: false, nErrors: errs});
        newState.eInvalid = true;
        newState.eValid = false;
        this.formIsValid(newState);
        return;
    }

    // Is nickname available?
    const xhr = new XMLHttpRequest();
    
    // Start spinner
    this.setState({spinner: true});
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 & xhr.status == 200) {
            if (xhr.response == "none") {
                this.setState({nValid: true, nInvalid: false, nErrors: []});
                newState.nInvalid = false;
                newState.nValid = true;
            } else {
                const errors = ["This nickname is already taken"];
                this.setState({nValid: false, nInvalid: true, nErrors: errors});
                newState.nInvalid = true;
                newState.nValid = false; 
            }
            this.formIsValid(newState);
            this.setState({spinner: false});
        }
    }
    
    xhr.open("POST", BACK_END + "/checkNick");
    xhr.withCredentials = true;
    xhr.send(contents);
}

export default checkNick;