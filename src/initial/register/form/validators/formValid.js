function formIsValid(newState) {
    let {pInvalid, eInvalid, nInvalid, pcInvalid, eValid, nValid, pValid, pcValid} = newState;
    
    // If undefined, don't care
    pInvalid = pInvalid == undefined ? false : pInvalid;
    eInvalid = eInvalid == undefined ? false : eInvalid;
    nInvalid = nInvalid == undefined ? false : nInvalid;
    pcInvalid = pcInvalid == undefined ? false : pcInvalid;
    
    pValid = pValid == undefined ? true : pValid
    nValid = nValid == undefined ? true : nValid
    eValid = eValid == undefined ? true : eValid
    pcValid = pcValid == undefined ? true : pcValid

    // Main handling
    if (eInvalid | pInvalid | nInvalid | pcInvalid) {
        this.setState({formInvalid: true, formValid: false});
    } else if(eValid & nValid & pValid & pcValid) {
        this.setState({formInvalid: false, formValid: true});
    }
}

export default formIsValid;