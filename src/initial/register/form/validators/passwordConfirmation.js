 // Validating password confirmation
 function checkPasswordConfirmation() {
    let newState = Object.assign({}, this.state);
    const password = this.password.current.value;
    const confirmation = this.passwordC.current.value; 
    if (!this.state.pValid) {
        this.setState({pcErrors: [], pcInvalid: false, pcValid: false});
        return;
    }
    if (confirmation != password) {
        const errs = ["Passwords don't match"];
        this.setState({pcErrors: errs, pcInvalid: true, pcValid: false});
        newState.pcInvalid = true;
        newState.pcValid = false;
    } else {
        this.setState({pcErrors: [], pcInvalid: false, pcValid: true});
        newState.pcInvalid = false;
        newState.pcValid = true;
    }
    this.formIsValid(newState);
}

export default checkPasswordConfirmation;