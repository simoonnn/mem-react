// Validating password
function checkPassword() {
    let newState = Object.assign({}, this.state);        
    let errors = [];
    const contents = this.password.current.value;
    const reUpper = /[A-Z]+/;
    const reLower = /[a-z]+/;
    const reNumber = /[1-9]+/;
    const specialChars = /[<>.(){}'"]+/

    // At least 8 chars 
    if (contents.length <= 7) {
        errors.push("At least 8 characters long")
    }

    // At least one uppercase character
    if (contents.match(reUpper) === null) {
        errors.push("At least one uppercase letter")            
    }

    // At least one lowercase character
    if (contents.match(reLower) === null) {
        errors.push("At least one lowercase letter");
    }

    // At least one number
    if (contents.match(reNumber) === null) {
        errors.push("At least one number");
    }

    if (contents.match(specialChars) != null) {
        errors.push("Not these: \"<>.(){}'\"");
    }

    
    if (errors.length > 0) {
        this.setState({pValid: false, pInvalid: true, pErrors: errors});
        newState.pInvalid = true;
        newState.pValid = false;
    } else {
        this.setState({pValid: true, pInvalid: false, pErrors: []});
        newState.pInvalid = false;
        newState.pValid = true;
    }
    this.formIsValid(newState);
    if (this.passwordC && this.passwordC.current.value != "") {
        this.checkPasswordConfirmation();
    }
}

export default checkPassword;