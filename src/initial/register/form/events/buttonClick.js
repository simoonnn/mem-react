import {BACK_END} from "presets";


// Submit the thing
function buttonClick() {
    const xhr = new XMLHttpRequest();

    // Construct the request body
    let body = {
        email: this.email.current.value.trim(),
        pass: this.password.current.value,
        passConfirm: this.passwordC.current.value,
        nickname: this.nickname.current.value.trim()
    } 

    // Stringify the body
    body = JSON.stringify(body);
    this.setState({spinner: true});
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 & xhr.status == 200) {
            const errorStatus = JSON.parse(xhr.response).status

            if (errorStatus == "none") {
                this.setState({popup: true});
            } else if (errorStatus == "email") {
                const errors = ["This email is already used"];

                this.setState({eErrors: errors, eInvalid: true, eValid: false, formValid: false, formInvalid: true})
            } else if (errorStatus == "nickname") {
                const errors = ["This nickname is already used"];

                this.setState({nErrors: errors, nInvalid: true, nValid: false, formValid: false, formInvalid: true})
            }
            this.setState({spinner: false});
        }
    }
    xhr.withCredentials = true;
    xhr.open("POST", BACK_END + "/register");
    xhr.send(body);
}
export default buttonClick;