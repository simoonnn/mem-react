function delayCheck(handler, timeout, ref) {
    let timer = null;
    let oldvalue = "";
    return () => {
        // Clear the previous timer
        clearTimeout(timer);
        let newValue = ref.current.value;
        
        //Don't set the time if value is the same
        if (newValue == oldvalue) {
            return;
        }
        
        // Set new timeout
        timer = setTimeout(() => {
            if (ref.current == null) {
                return;
            }
            
            handler();
            oldvalue = newValue;
        }, timeout);
    }
}

export default delayCheck;