// el selects what exactly to hide. It will set the according name of the state to the needed value.

function handleCheck(el) {
    return (e) => {
        let newState = {};
        if (e.target.checked) {
            newState[el] = false;
        } else {
            newState[el] = true;
        }

        this.setState(newState);
    }
} 

export default handleCheck;