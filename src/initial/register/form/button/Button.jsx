import PropTypes from "prop-types";
import React from "react";


import styles from "./styles.scss";

const Button = (props) => {
    const className = props.valid ? styles.Valid : styles.Button;
    return <div className={styles.Container}>
        <button disabled={!props.valid} onClick={props.handler} className={className}>{props.label}</button>
    </div>
    
}

Button.propTypes = {
    label: PropTypes.string.isRequired,
    valid: PropTypes.bool,
    handler: PropTypes.func
}


export default Button;