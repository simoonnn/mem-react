import React from "react";


import styles from "./styles.scss";

class Popup extends React.Component {
    constructor(props) {
        super(props);
        this.containerRef = React.createRef();

        // Bind methods 
        this.closePopup = this.closePopup.bind(this);
    }
    //Close the popup on click
    closePopup() {
        let classList = this.containerRef.current.classList;
        classList.remove("show");
        classList.add("hide");
        //Set to hidden after some time
        setTimeout(() => {
            classList.add("hidden");
            classList.remove("hide");
            this.props.onClose();
        }, 650);
    }
    render() {
        return <div ref={this.containerRef} className={styles.Container}>
            <h1>{this.props.text}</h1>
            <span onClick={this.closePopup} className={styles.close}>&times;</span>
        </div>
    }
}

export default Popup;