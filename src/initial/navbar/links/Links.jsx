import PropTypes from "prop-types";
import React from "react";
import {Link} from "react-router-dom";


import styles from "./styles.scss";

class Links extends React.Component {
    render() {
        const links = this.props.objectList.map((linkObject, index) => {
            const active = linkObject.hasOwnProperty("active");
            let className = active ? styles.Active : styles.Link;

            return <Link to={linkObject.to} key={index} className={className}>{linkObject.name}</Link>
        });
        return <div className={styles.Container}>
            {links}
        </div>
    }
}

Links.propTypes = {
    objectList: PropTypes.array.isRequired
}
export default Links;