import PropTypes from "prop-types";
import React from "react";

import Menu from "./menu/Menu";
import Links from "./links/Links";
import styles from "./styles.scss";

class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
        //Handler
        this.openMenu = this.openMenu.bind(this);
    }
    openMenu() {
        this.setState({open: !this.state.open});
    }
    render() {
        return <div className={styles.Container}>
            <Menu handler={this.openMenu} open={this.state.open}/>
            <Links objectList={this.props.objectList}/>
        </div>
    }
}

Navbar.propsTypes = {
    objectList: PropTypes.array.isRequired
}

export default Navbar;