import React from "react";


import styles from "./styles.scss";

class Menu extends React.Component {
    render() {
        const className = this.props.open ? styles.ContainerOn : styles.Container;
        return <div onClick={this.props.handler} className={className}>
            <img alt="Menu icon" src="/assets/icons/navbar/menu.svg"/>
        </div>
    }
}

export default Menu;