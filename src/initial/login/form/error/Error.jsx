import PropTypes from "prop-types";
import React from "react";


import styles from "./styles.scss";

class Error extends React.Component {
    render() {
        return <div className={styles.Container}>
            <span className={styles.Error}>{this.props.message}</span>
        </div>
    }
}

Error.propTypes = {
    message: PropTypes.string.isRequired
}

export default Error;