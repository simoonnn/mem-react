import React from "react";
import {Redirect} from "react-router-dom";

// Components 
import Spinner from "utils/spinner/Spinner-container";
import Error from "./error/Error";
import { Email, Password } from "register/form/fields/Fields";
import { Header1 } from "register/form/headers/Headers";
import Button from "register/form/button/Button";

// Handlers
import validatePassword from "register/form/validators/password";
import validateEmail from "register/form/validators/email";
import formIsValid from "register/form/validators/formValid";
import delayCheck from "register/form/events/delay";

// Constants 
import {BACK_END, LOGIN_COOKIE_EXPIRATION} from "presets";

// Styles
import styles from "./styles.scss";

class Form extends React.Component {
    constructor(props) {
        super(props);

        // references
        this.email = React.createRef();
        this.password = React.createRef();

        // state
        this.state = {
            eInvalid: false,
            eValid: false,
            eErrors: [],
            pInvalid: false,
            pValid: false,
            pHidden: true,
            pErrors: [],
            formValid: false,
            serverStatus: "success",
            redirect: false,
            spinner: false
        }
        // handlers
        this.handleCheckbox = this.handleCheckbox.bind(this);
        this.formIsValid = formIsValid.bind(this);
        this.buttonHandler = this.buttonHandler.bind(this);
        const emailValidator = validateEmail.bind(this);
        const passwordValidator = validatePassword.bind(this);

        // handler objects
        this.emailHandler = {   
            onInput: delayCheck(emailValidator, 600, this.email)
        };
        this.passwordHandler = {   
            onInput: delayCheck(passwordValidator, 600, this.password)
        };
    
    }

    handleCheckbox() {
        this.setState({pHidden: !this.state.pHidden})
    }

    buttonHandler(e) {
        e.preventDefault();
        // send datas
        const xhr = new XMLHttpRequest();
        this.setState({spinner: true});
        const email = this.email.current.value;

        xhr.onreadystatechange = () => {
            if (xhr.status == 200 && xhr.readyState == 4) {
                const response = JSON.parse(xhr.response);
                if (response.status == "success") {
                    const data = new Date();
                    const expiration_date = new Date(data.getTime() + LOGIN_COOKIE_EXPIRATION);
                    const expiration_mls = expiration_date.toGMTString();

                    // Set cookies, verifying the user is authenticated
                    const session_token = response.session_token;
                    document.cookie=`nickname=${response.nickname};expires=${expiration_mls}`;
                    document.cookie=`token=${session_token};expires=${expiration_mls}`;


                    this.setState({redirect: true});
                    return;
                }
                this.setState({serverStatus: response.status, spinner: false})
            }
        };

        xhr.open("POST", BACK_END + "/login");

        // construct body
        const bodyObject = {
            email: email.trim(),
            password: this.password.current.value
        }
        const body = JSON.stringify(bodyObject);

        xhr.send(body);
    }

    render() {
        const state = this.state;
        const spinner = state.spinner;
        // Message to display
        let message;
        if (state.serverStatus == "server") {
            message = "Server error. Please try again later.";
        } else if (state.serverStatus == "creds") {
            message = "Invalid email/password combination";
        }   
        const showError = state.serverStatus != "success";

        if (state.redirect) {
            return <Redirect to="/"/>
        }        

        return <div      className={styles.Form}>
            <Header1 label="Login" />
            { spinner ? <Spinner /> : "" }
            {showError ? <Error message={message} /> : ""}
            <Email ref={this.email} handler={this.emailHandler} invalid={this.state.eInvalid} valid={state.eValid} errorList={state.eErrors}/>
            <Password ref={this.password} hidden={this.state.pHidden} label="Password" handleCheck={this.handleCheckbox} handler={this.passwordHandler} invalid={state.pInvalid} valid={state.pValid} errorList={state.pErrors}/>
            <Button handler={this.buttonHandler} label="Log in" valid={state.formValid}/>
        </div>
    }
}

export default Form;