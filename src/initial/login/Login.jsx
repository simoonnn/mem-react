import React from "react";


import Form from "./form/Form";
import Navbar from "../navbar/Navbar";
import "./styles.scss";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.objectList = [
            {to: "/home", name: "Home"},
            {to: "/register", name: "Register"},
            {to: "/login", name: "Login", active: true}
        ];
    }
    render() {
        return <div>
                <Navbar objectList={this.objectList} />
                <Form/>
            </div>
    }
}

export default Login;