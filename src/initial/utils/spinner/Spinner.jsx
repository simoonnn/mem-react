import React from "react";
import PT from "prop-types";
import classNames from "classnames";

import styles from "./styles.scss";

class Spinner extends React.Component {
    render() {
        const className = classNames(styles.Spinner, this.props.cls);

        return <div className={className}>
        </div>
    }
}

Spinner.propTypes = {
    cls: PT.string
}

export default Spinner;
