import React from "react";

import Spinner from "./Spinner";
import styles from "./styles.scss";

class SpinnerContainer extends React.Component {
    render() {
        return <div className={styles.Container}>
            <Spinner cls={styles.Spin}/>
        </div>
    }
}
export default SpinnerContainer;
