import React from "react";
import { Link } from "react-router-dom";

import styles from "./styles.scss";
import Navbar from "../navbar/Navbar";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.objectList = [
            { to: "/home", name: "Home", active: true },
            { to: "/register", name: "Register" },
            { to: "/login", name: "Login" }
        ];
    }
    render() {
        return <div className={styles.MainContainer}>
            <Navbar objectList={this.objectList} />
            <div className={styles.Container}>
                <h1>Hello</h1>
                <p>In order to use our website, </p>
                <p>please <Link to="/login">Login</Link> or <Link to="/register">Register</Link></p>
            </div>
        </div>
    }
}

export default Home;