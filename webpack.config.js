path = require("path");
webpack = require("webpack");
htmlPlugin = require("html-webpack-plugin");

module.exports = {
    mode: "development",
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "assets"),
        filename: "bundle.js",
        publicPath: "/assets/"
    },
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: [
                    path.resolve(__dirname, "node-modules")
                ],
                loader: "babel-loader",
                options: {
                    presets: ["react"]
                }
            },
            {
                test: /\.css/,
                use: [
                    "style-loader", 
                    {
                        loader: "css-loader",
                        options: {
                            modules: true
                        }
                    }
                ]
            },
            {
                test: /\.scss/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: {
                                localIdentName: '[path][name]_[local]--[hash:base64:5]'
                            }
                        }
                    },
                    "sass-loader",
                ]
            },
            {
                test: /\.(png|jpg|otf|ttf|svg)$/,
                loader: "file-loader"
            }
        ]
    },
    resolve: {
        extensions: [".css", ".js",".scss", ".jsx", ".html"],
        modules: [
            "node_modules",
            path.resolve(__dirname, "src"),
            path.resolve(__dirname, "src", "initial"),
            path.resolve(__dirname, "src", "logged")
        ],
        alias: {
            src: path.resolve(__dirname, "src") 
        }
    },
    devServer: {
        hot: true,
        contentBase: __dirname,
        historyApiFallback: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
}
